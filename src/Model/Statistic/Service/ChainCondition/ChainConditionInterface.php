<?php


namespace App\Model\Statistic\Service\ChainCondition;


interface ChainConditionInterface
{


    public function getConditionByDate(ChainCondition $chainCondition);

}
