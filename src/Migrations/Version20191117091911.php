<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191117091911 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signs ADD wla_planet_2_id INT DEFAULT NULL, ADD izg_planet_2_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81FAF0C717 FOREIGN KEY (wla_planet_2_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E816D74E82A FOREIGN KEY (izg_planet_2_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_41946E81FAF0C717 ON signs (wla_planet_2_id)');
        $this->addSql('CREATE INDEX IDX_41946E816D74E82A ON signs (izg_planet_2_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81FAF0C717');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E816D74E82A');
        $this->addSql('DROP INDEX IDX_41946E81FAF0C717 ON signs');
        $this->addSql('DROP INDEX IDX_41946E816D74E82A ON signs');
        $this->addSql('ALTER TABLE signs DROP wla_planet_2_id, DROP izg_planet_2_id');
    }
}
