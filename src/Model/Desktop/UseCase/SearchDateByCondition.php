<?php
namespace App\Model\Desktop\UseCase;

use Doctrine\ORM\EntityManagerInterface;

class SearchDateByCondition
{

    private $entityManager;
    private $connection;

    public function __construct(EntityManagerInterface $entityManager)
    {

        $this->entityManager = $entityManager;
        $this->connection = $this->entityManager->getConnection();

    }

    public function searchDateByCondition($conditions, $param){

        // Definition params

        if(count($conditions) == 0) return [];
        $chain_type_id = $param["chain_type_id"];
        $first_planet_id = $conditions[0]->planet;
        $first_sign_query = "";
        if(isset($conditions[0]->sign)){
            if((bool)$conditions[0]->sign){
                $first_sign_query = " and join_alias.sign_id = " . $conditions[0]->sign;
            }
        }
        $first_orbit_query = "";
        if(isset($conditions[0]->orbit)){
            if($conditions[0]->orbit != ""){
                $first_orbit_query = " and join_alias.orbit = " . (int)$conditions[0]->orbit;
            }
        }

        // Build query

        $sql = $this->getStartQuery($chain_type_id, $first_planet_id);
        foreach ($conditions as $key => $condition){
            if($key == 0) continue;
            $sql .= $this->getJoinPartQuery($condition, $key, $chain_type_id);
        }
        $sql .= $this->getWhereQuery($first_sign_query, $first_orbit_query, $conditions);
        $sql .= " LIMIT 1";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    private function getStartQuery($chain_type_id, $first_planet_id){
        return "
            SELECT join_alias.day_id
            FROM (SELECT planet_indexes.planet_id, planet_indexes.sign_id, oi.orbit, planet_indexes.day_id
                  FROM planet_indexes
                           JOIN orbit_indexes oi
                                on planet_indexes.day_id = oi.day_id and planet_indexes.planet_id = oi.planet_id
                  WHERE oi.chain_type_id = $chain_type_id and planet_indexes.planet_id =$first_planet_id
                 ) as join_alias";
    }

    private function getJoinPartQuery($condition, $key, $chain_type_id){
        $planet_id = $condition->planet;
        return "
                  join
                 (SELECT planet_indexes.planet_id, planet_indexes.sign_id, oi.orbit, planet_indexes.day_id
                  FROM planet_indexes
                           JOIN orbit_indexes oi
                                on planet_indexes.day_id = oi.day_id and planet_indexes.planet_id = oi.planet_id
                  WHERE oi.chain_type_id = $chain_type_id and planet_indexes.planet_id = $planet_id
                 ) as join_$key ON join_alias.day_id = join_$key.day_id
        ";
    }

    private function getWhereQuery($first_sign_query, $first_orbit_query, $conditions){
        $sql = "    WHERE join_alias.day_id is NOT NULL $first_sign_query  $first_orbit_query";

        foreach ($conditions as $key => $condition){
            if($key == 0) continue;
            $sign_query = "";
            if(isset($condition->sign)){
                if((bool)$condition->sign){
                    $sign_query = " and join_$key.sign_id = " . $condition->sign;
                }
            }
            $orbit_query = "";
            if(isset($condition->orbit)){
                if($condition->orbit != ""){
                    $orbit_query = " and join_$key.orbit = " . (int)$condition->orbit;
                }
            }

            $sql .= " $sign_query  $orbit_query";

        }
        return $sql;
    }
}
