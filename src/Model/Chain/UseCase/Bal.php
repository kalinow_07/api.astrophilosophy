<?php

namespace App\Model\Chain\UseCase;

use App\Model\Chain\Entity\Sign;
use App\Repository\SignRepository;
use App\Model\Chain\Entity\Planet as PlanetEntity;

class Bal
{

    private $planet;
    private $bal;


    public function get(PlanetEntity $planet){

        $planet_sign_marker = $planet->sign;

        $sign = $planet->getWlaSigns()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();

        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 6;
            }
        }

        $sign = $planet->getWla2Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();

        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 6;
            }
        }

        $sign = $planet->getEkzSigns()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();

        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 5;
            }
        }

        $sign = $planet->getRod1Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 4;
            }
        }

        $sign = $planet->getRod2Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 4;
            }
        }

        $sign = $planet->getRod3Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 4;
            }
        }

        $sign = $planet->getNetSigns()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 3;
            }
        }

        $sign = $planet->getWra1Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 2;
            }
        }

        $sign = $planet->getWra2Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 2;
            }
        }

        $sign = $planet->getWra3Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 2;
            }
        }

        $sign = $planet->getPadSigns()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 1;
            }
        }

        $sign = $planet->getIzgSigns()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 1;
            }
        }

        $sign = $planet->getIzg2Signs()->filter(function (Sign $sign) use ($planet_sign_marker){
            return $sign->marker == $planet_sign_marker;
        })->first();
        if(gettype($sign) != "boolean"){
            if($sign->getMarker() == $planet_sign_marker){
                return 1;
            }
        }

        return null;

    }

}