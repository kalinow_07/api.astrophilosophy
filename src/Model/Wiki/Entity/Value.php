<?php

namespace App\Model\Wiki\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Model\Wiki\Repository\ValueRepository")
 */

class Value
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wikibase_item;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $name;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $column1;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $column2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_rus;


}