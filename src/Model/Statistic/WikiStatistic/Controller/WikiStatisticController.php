<?php

namespace App\Model\Statistic\WikiStatistic\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\FourWindow\UseCase\EventWindow\EventWindow;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForOrbit;
use App\Model\Statistic\PlanetStatistic\UseCase\GetPlanetStatistic;
use App\Model\Statistic\PlanetStatistic\UseCase\GetPlanetStatistic\Request\Command;
use App\Model\Statistic\Service\ChainCondition\ChainCondition;
use App\Model\Statistic\WikiStatistic\UseCase\GetChartWikiStatistic;
use App\Model\Statistic\WikiStatistic\UseCase\GetGroupsWikiStatistic;
use App\Model\Statistic\WikiStatistic\UseCase\GetValuesWikiStatistic;
use App\Model\Wiki\Entity\CategoryProperty;
use App\Model\Wiki\Entity\Value;
use App\Repository\PlanetRepository;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WikiStatisticController extends AbstractController
{

    /**
     * @Route("/wiki_statistic/get_groups", name="wiki_statistic_get_groups", methods={"GET"})
     */
    public function getGroups(Request $request, GetGroupsWikiStatistic $getGroupsWikiStatistic)
    {
        try{

            $chain_condition_input = $request->query->get("chain_condition");
            $param = $request->query->all();
            $chain_condition = new ChainCondition($chain_condition_input);
            $result = $getGroupsWikiStatistic->get($chain_condition, $param);

            $response = new JsonResponse($result);
            //$response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/wiki_statistic/get_values", name="wiki_statistic_get_values", methods={"GET"})
     */
    public function getValues(Request $request, GetValuesWikiStatistic $getValuesWikiStatistic)
    {
        try{

            $chain_condition_input = $request->query->get("chain_condition");
            $param = $request->query->all();
            $chain_condition = new ChainCondition($chain_condition_input);
            $result = $getValuesWikiStatistic->get($chain_condition, $param);


            $response = new JsonResponse($result);
            //$response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/wiki_statistic/get_chart", name="wiki_statistic_get_chart", methods={"GET"})
     */
    public function getChart(Request $request, GetChartWikiStatistic $getChartWikiStatistic)
    {
        try{

            $chain_condition_input = $request->query->get("chain_condition");
            $param = $request->query->all();
            $chain_condition = new ChainCondition($chain_condition_input);
            $result = $getChartWikiStatistic->get($chain_condition, $param);


            $response = new JsonResponse($result);
            //$response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }

}
