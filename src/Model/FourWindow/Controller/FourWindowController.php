<?php

namespace App\Model\FourWindow\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForOrbit;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForSign;
use App\Model\Wiki\Repository\PropertyRepository;
use App\Repository\PlanetRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FourWindowController extends AbstractController
{


    /**
     * @Route("/four_window/setting/get", name="four_window_setting_get")
     */
    public function getByOrbit(Request $request, PlanetRepository $planetRepository,
                               PropertyRepository $propertyRepository,
                               SignRepository $signRepository)
    {
        try{

            $result = [
                "planets" => $planetRepository->getPublicList(),
                "signs" => $signRepository->findAll(),
                "properties" => $propertyRepository->findAll(),
            ];

            $response = new JsonResponse($result);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()], JSON_UNESCAPED_UNICODE);
        }

    }

}
