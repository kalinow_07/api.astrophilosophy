<?php

namespace App\Repository;

use App\Model\Chain\Entity\Sign;
use App\Model\User\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return Sign[] Returns an array of Sign objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sign
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getUserByUserId(int $user_id): ?User
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.user_id = :user_id')
            ->setParameter('user_id', $user_id)
            ->getQuery();
        $sign = $qb->getOneOrNullResult();
        return $sign;
    }

    public function getByCode(string $code): ?User
    {
        return $this->createQueryBuilder('p')
            ->where('p.code = :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getOneOrNullResult();
    }
//
//    public function getByMarker($marker){
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.marker = :marker')
//            ->setParameter('marker', $marker)
//            ->getQuery()
//            ->getOneOrNullResult();
//    }


}
