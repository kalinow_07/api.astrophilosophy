<?php

namespace App\Model\FourWindow\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForOrbit;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForSign;
use App\Repository\PlanetRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PlanetController extends AbstractController
{


    /**
     * @Route("/four_window/planet/get_by_orbit", name="four_window_planet_get_by_orbit")
     */
    public function getByOrbit(Request $request, PlanetWindowForOrbit $planetWindow)
    {
        try{

            $param = $request->query->all();
            $result = $planetWindow->get($param);

            $response = new JsonResponse($result);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()], JSON_UNESCAPED_UNICODE);
        }

    }

    /**
     * @Route("/four_window/planet/get_by_sign", name="four_window_planet_get_by_sign")
     */
    public function getBySign(Request $request, PlanetWindowForSign $planetWindowForSign)
    {
        try{

            $param = $request->query->all();
            $result = $planetWindowForSign->get($param);

            $response = new JsonResponse($result);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }

    }
}
