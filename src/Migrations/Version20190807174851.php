<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190807174851 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category_values (id INT AUTO_INCREMENT NOT NULL, category_property_id INT DEFAULT NULL, value_id INT DEFAULT NULL, INDEX category_idx (category_property_id), INDEX value_idx (value_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_properties (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, property VARCHAR(255) NOT NULL, INDEX property_idx (property), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_values ADD CONSTRAINT FK_354F869DE3095187 FOREIGN KEY (category_property_id) REFERENCES category_properties (id) ON DELETE RESTRICT');
        $this->addSql('ALTER TABLE category_values ADD CONSTRAINT FK_354F869DF920BBA2 FOREIGN KEY (value_id) REFERENCES value (id) ON DELETE RESTRICT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category_values DROP FOREIGN KEY FK_354F869DE3095187');
        $this->addSql('DROP TABLE category_values');
        $this->addSql('DROP TABLE category_properties');
    }
}
