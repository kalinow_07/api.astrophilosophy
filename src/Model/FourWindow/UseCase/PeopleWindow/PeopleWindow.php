<?php

namespace App\Model\FourWindow\UseCase\PeopleWindow;

use App\Model\Chain\Entity\Planet;
use App\Model\FourWindow\Controller\PlanetController;
use App\Repository\PlanetRepository;
use App\Repository\SignRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PeopleWindow
{

    /**
     * @Assert\NotBlank
     */
    private $value_id;
    /**
     * @Assert\NotBlank
     */
    private $date_from;

    /**
     * @Assert\NotBlank
     */
    private $date_to;

    /**
     * @Assert\NotBlank
     */
    private $property;

    /**
     * @Assert\NotBlank
     */
    private $first_planet_id;
    private $first_planet;

    private $first_sign_id;
    private $first_sign;

    private $first_orbit;

    private $second_planet_id;
    private $second_planet;

    private $second_sign_id;
    private $second_sign;

    private $second_orbit;

    private $conditions = " AND v.name_rus != '' ";

    private $limit = 20;
    private $page = 1;
    private $offset = 0;
    private $count_all = 300;

    public $entityManager;
    public $validator;
    private $connection;
    private $planetRepository;
    private $signRepository;

    public function __construct(EntityManagerInterface $entityManager,
                                ValidatorInterface $validator,
                                PlanetRepository $planetRepository,
                                SignRepository $signRepository)
    {

        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->connection = $this->entityManager->getConnection();
        $this->planetRepository = $planetRepository;
        $this->signRepository = $signRepository;

    }

    public function get(array $param){

        if(isset($param["value_id"])){
            $this->value_id = $param["value_id"];
        }
        if(isset($param["date_from"])){
            $this->date_from = $param["date_from"];
        }
        if(isset($param["date_to"])){
            $this->date_to = $param["date_to"];
        }
        if(isset($param["property"])){
            $this->property = $param["property"];
        }
        if(isset($param["first_planet_id"])){
            $this->first_planet_id = $param["first_planet_id"];
            $this->first_planet = $this->planetRepository->find($this->first_planet_id);
            if(!(bool)$this->first_planet) throw new Exception("First planet not find");
        }
        if(isset($param["second_planet_id"])){
            $this->second_planet_id = $param["second_planet_id"];
            $this->second_planet = $this->planetRepository->find($this->second_planet_id);
            if(!(bool)$this->second_planet) throw new Exception("Second planet not find");
        }


        if(isset($param["first_sign_id"])){
            $this->first_sign_id = $param["first_sign_id"];
            $this->first_sign = $this->signRepository->find($this->first_sign_id);
            if(!(bool)$this->first_sign) throw new Exception("First sign not find");
            $this->conditions .= " AND sign_" . $this->first_planet->getMarker() . " = '" . $this->first_sign->getMarker() . "' ";
        }
        if(isset($param["first_orbit"])){
            $this->first_orbit = $param["first_orbit"];
            $this->conditions .= " AND orb_" . $this->first_planet->getMarker() . " = " . $param["first_orbit"] . " ";
        }


        if(isset($param["second_orbit"])){
            $this->second_orbit = $param["second_orbit"];
            if(!(bool)$this->second_planet) throw new Exception("Second planet not find");
            $this->conditions .= " AND orb_" . $this->second_planet->getMarker() . " = " . $param["second_orbit"] . " ";
        }
        if(isset($param["second_sign_id"])){
            $this->second_sign_id = $param["second_sign_id"];
            $this->second_sign = $this->signRepository->find($this->second_sign_id);
            if(!(bool)$this->second_sign) throw new Exception("Second sign not find");
            if(!(bool)$this->second_planet) throw new Exception("Second planet not find");
            $this->conditions .= " AND sign_" . $this->second_planet->getMarker() . " = '" . $this->second_sign->getMarker() . "' ";
        }

        if(isset($param["limit"])){
            $this->limit = $param["limit"];
        }

        if(isset($param["page"])){
            $this->page = $param["page"];
        }

        $this->offset = ($this->page - 1) * $this->limit;


        $this->validation();

        $sql = "
            SELECT peoples.title, peoples.pageid, peoples.wikibase_item, birthday_date
            FROM peoples
                   JOIN people_property on peoples.id = people_property.people_id
                   JOIN value v on people_property.value_id = v.id
                   JOIN chains ON chains.date = peoples.birthday_date
            WHERE property = :property
              $this->conditions
              AND chains.date > :date_from
              AND chains.date < :date_to
              AND v.id = :value_id
            GROUP BY peoples.id
        ";

        $sql_for_items = $sql . " LIMIT $this->limit OFFSET $this->offset";
        $stmt_for_items = $this->connection->prepare($sql_for_items);
        $stmt_for_items->execute($this->getConditions());

        $stmt_for_count = $this->connection->prepare($sql);
        $stmt_for_count->execute($this->getConditions());


        return [
            "items" => $stmt_for_items->fetchAll(),
            "count" => count($stmt_for_count->fetchAll()),
        ];

    }

    private function validation(){
        $errors = $this->validator->validate($this);
        if(count($errors) > 0){
            throw new Exception($errors[0]->getMessage());
        }
    }

    private function getConditions(){

        $conditions = [];

        $conditions["date_from"] = $this->date_from;
        $conditions["date_to"] = $this->date_to;
        $conditions["property"] = $this->property;
        $conditions["value_id"] = $this->value_id;

        return $conditions;
    }
}