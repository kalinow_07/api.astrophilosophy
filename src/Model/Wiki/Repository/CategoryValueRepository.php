<?php

namespace App\Model\Wiki\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Model\Wiki\Entity\CategoryValue;

/**
 * @method CategoryValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryValue[]    findAll()
 * @method CategoryValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryValueRepository extends ServiceEntityRepository
{


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoryValue::class);
    }

    // /**
    //  * @return Chain[] Returns an array of Chain objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chain
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
