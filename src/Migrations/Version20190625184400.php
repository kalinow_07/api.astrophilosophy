<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190625184400 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE people_property DROP FOREIGN KEY FK_FA03304B3147C936');
        $this->addSql('ALTER TABLE people_property CHANGE value_id value_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE people_property ADD CONSTRAINT FK_FA03304BF920BBA2 FOREIGN KEY (value_id) REFERENCES value (id) ON DELETE RESTRICT');
        $this->addSql('ALTER TABLE people_property ADD CONSTRAINT FK_FA03304B3147C936 FOREIGN KEY (people_id) REFERENCES peoples (id) ON DELETE RESTRICT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE people_property DROP FOREIGN KEY FK_FA03304BF920BBA2');
        $this->addSql('ALTER TABLE people_property DROP FOREIGN KEY FK_FA03304B3147C936');
        $this->addSql('ALTER TABLE people_property CHANGE value_id value_id INT NOT NULL');
        $this->addSql('ALTER TABLE people_property ADD CONSTRAINT FK_FA03304B3147C936 FOREIGN KEY (people_id) REFERENCES peoples (id) ON DELETE CASCADE');
    }
}
