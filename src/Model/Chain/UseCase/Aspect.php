<?php

namespace App\Model\Chain\UseCase;

use App\Repository\AspectRepository;
use App\Repository\SignRepository;
use App\Model\Chain\Entity\Planet as PlanetEntity;
use Psr\Log\LoggerInterface;

class Aspect
{

    private $aspectRepository;
    private $aspects;
    private $logger;

    private $planet1;
    private $planet2;
    private $aspect;
    private $result;

    public function __construct(AspectRepository $aspectRepository, LoggerInterface $logger)
    {
        $this->aspectRepository = $aspectRepository;
        $this->aspects = $this->aspectRepository->findAll();
        $this->logger = $logger;

    }

    public function getAspect(PlanetEntity $planet1, PlanetEntity $planet2){

        $this->result = null;

        $this->planet1 = $planet1;
        $this->planet2 = $planet2;

        foreach ($this->aspects as $aspect){
            $this->aspect = $aspect;
            $this->calculate();
        }

        return $this->result;

    }

    private function calculate(){

        $d = $this->aspect->getDegree(); // градус аспекта
        $p = $this->aspect->getInaccuracy(); // погрешность аспекта
        $x = $this->planet1->degree; // Градус первой планеты
        $dp = $this->planet2->degree; // Градус второй планеты


        // 1 В ОДНУ СТОРОНУ
        // 1.1 Вычислить градус, в который может попасить другая планета

        $seam = $x + $d;
        if($seam >= 360){
            // 1.1.1 Если попадает на стык
            $z = (360-$x)+($d-(360-$x));
        }else{
            // 1.1.2 Если не попадает на стык
            $z = $x + $d;
        }


        // 1.2.1 Вычисление минимальной границы погрешности
        $seam = $z - ($z -$p);
        if($seam > 250){
            // 1.2.1.1 Если попадает на стык
            $p_min = (360 - ($p - $z));
        }else{
            // 1.2.1.2 Если не попадает на стык
            $p_min = $z - $p;
        }


        // 1.2.2 Вычисление минимальной границы погрешности
        $seam = $z + $p;
        if($seam > 360){
            // 1.2.2.1 Если попадает на стык
            $p_max = $p - (360 - $z);
        }else{
            // 1.2.2.2 Если не попадает на стык
            $p_max = $z + $p;
        }

        // 1.3 Определить аспект
        if($dp > $p_min && $dp < $p_max){
            $this->result = [
                "planet1" => $this->planet1->getMarker(),
                "planet2" => $this->planet2->getMarker(),
                "aspect" => $this->aspect->getMarker(),
                "id" => $this->aspect->getId(),
                "name" => $this->aspect->getName(),
            ];

        }





























    }
}
