<?php

namespace App\Model\Chain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrbitIndexRepository")
 * @ORM\Table(name="orbit_indexes", uniqueConstraints={@ORM\UniqueConstraint(name="IDX_PLANET_DAY", columns={"planet_id", "day_id", "chain_type_id"})})
 * @UniqueEntity(
 *     fields={"planet_id", "day_id", "chain_type_id"},
 *     errorPath="planet_day_chain_type_uniq",
 *     message="Error uniq planet and day and, chain_type."
 * )
 */
class OrbitIndex
{


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="integer", precision=10, scale=5)
     */
    private $orbit;

    /**
     * @ORM\Column(type="integer", precision=10, scale=5, nullable=true)
     */
    private $bal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="orbit_indexes")
     * @ORM\JoinColumn(name="planet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $planet;


    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\ChainType", inversedBy="orbit_indexes")
     * @ORM\JoinColumn(name="chain_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $chain_type;


    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Day", inversedBy="orbit_indexes")
     * @ORM\JoinColumn(name="day_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $day;

    /**
     * @return mixed
     */
    public function getOrbit()
    {
        return $this->orbit;
    }

    /**
     * @param mixed $orbit
     */
    public function setOrbit($orbit): void
    {
        $this->orbit = $orbit;
    }

    /**
     * @return mixed
     */
    public function getBal()
    {
        return $this->bal;
    }

    /**
     * @param mixed $bal
     */
    public function setBal($bal): void
    {
        $this->bal = $bal;
    }

    /**
     * @return mixed
     */
    public function getPlanet()
    {
        return $this->planet;
    }

    /**
     * @param mixed $planet
     */
    public function setPlanet($planet): void
    {
        $this->planet = $planet;
    }

    /**
     * @return mixed
     */
    public function getChainType()
    {
        return $this->chain_type;
    }

    /**
     * @param mixed $chain_type
     */
    public function setChainType($chain_type): void
    {
        $this->chain_type = $chain_type;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day): void
    {
        $this->day = $day;
    }




}
