<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 23.06.19
 * Time: 19:50
 */

namespace App\Model\Chain\UseCase;

use App\Model\Chain\Entity\Planet;
use Symfony\Component\Config\Definition\Exception\Exception;

class Center
{

    private $result_array = []; // Результирующий массив

    private $times = 20; // Сколько раз запустить функцию определения орбиты.
    private $time_iterations = 1;

    private $planets = [];
    private $original_planets;

    private $centers = [];

    public function __construct(array $planets)
    {
        $this->original_planets = $planets;

        foreach ($planets as $planet) {
            $this->planets[$planet->marker] = $planet->parent;
        }
    }

    public function get(Planet $planet)
    {

        $this->centers = [];

        foreach ($this->original_planets as $original_planet){
            $this->time_iterations = 1;
            $this->result_array = [];
            $this->iteration($original_planet->marker);
            $this->centers[] = $this->result_array;
        }

        array_multisort(array_map('count', $this->centers), SORT_DESC, $this->centers);

        foreach ($this->centers as $key => $center){
            foreach ($this->centers as $key2 => $center2){
                if($key2 <= $key) continue;
                if($center === $center2) continue;
                $intersect = array_intersect_key($center, $center2);
                if(count($intersect) != 0){
                    unset($this->centers[$key2]);
                }
            }
        }

        $this->centers = array_values($this->centers);


        return $this->getCenterNumber($planet);

    }

    private function iteration($planet_marker)
    {
        $this->time_iterations++;
        if ($this->time_iterations > $this->times) return true;
        $this->addToResultArray($planet_marker);
        $this->iteration($this->planets[$planet_marker]);

    }

    private function addToResultArray($planet_marker)
    {

        // Если в нет в массиве, то добавляем новый элемент, иначе прибавляем единицу
        if (!isset($this->result_array[$planet_marker])) {
            $this->result_array[$planet_marker] = 1;
        } else {
            $this->result_array[$planet_marker]++;
        }
    }

    private function getCenterNumber($planet){

        //var_dump($this->centers);

        if($planet->orbit != 0) return 0;

        foreach ($this->centers as $key => $center){

            if(array_key_exists($planet->marker, $center)){
                return $key + 1;
            }
        }

        throw new Exception("Номер центра не определен");
    }




}