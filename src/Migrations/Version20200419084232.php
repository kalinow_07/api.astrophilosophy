<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200419084232 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE orbit_indexes (id INT AUTO_INCREMENT NOT NULL, planet_id INT DEFAULT NULL, chain_type_id INT DEFAULT NULL, day_id INT DEFAULT NULL, orbit INT NOT NULL, bal INT NOT NULL, INDEX IDX_2D7B125EA25E9820 (planet_id), INDEX IDX_2D7B125E6E3C6417 (chain_type_id), INDEX IDX_2D7B125E9C24126 (day_id), UNIQUE INDEX IDX_PLANET_DAY (planet_id, day_id, chain_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orbit_indexes ADD CONSTRAINT FK_2D7B125EA25E9820 FOREIGN KEY (planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE orbit_indexes ADD CONSTRAINT FK_2D7B125E6E3C6417 FOREIGN KEY (chain_type_id) REFERENCES chain_types (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE orbit_indexes ADD CONSTRAINT FK_2D7B125E9C24126 FOREIGN KEY (day_id) REFERENCES days (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE orbit_indexes');
    }
}
