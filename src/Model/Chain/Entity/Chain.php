<?php


namespace App\Model\Chain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ChainRepository")
 * @ORM\Table(name="chains",indexes={@Index(name="date_idx", columns={"date"})})
 */
class Chain
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_sol;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_lun;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_mer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_wen;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_mar;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_jup;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_sat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_ura;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_nep;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_plu;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_use1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orb_use2;



    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_sol;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_lun;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_mer;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_wen;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_mar;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_jup;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_sat;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_ura;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_nep;

    /**
     * @return mixed
     */
    public function getOrbUse1()
    {
        return $this->orb_use1;
    }

    /**
     * @param mixed $orb_use1
     */
    public function setOrbUse1($orb_use1): void
    {
        $this->orb_use1 = $orb_use1;
    }

    /**
     * @return mixed
     */
    public function getOrbUse2()
    {
        return $this->orb_use2;
    }

    /**
     * @param mixed $orb_use2
     */
    public function setOrbUse2($orb_use2): void
    {
        $this->orb_use2 = $orb_use2;
    }

    /**
     * @return mixed
     */
    public function getSignUse1()
    {
        return $this->sign_use1;
    }

    /**
     * @param mixed $sign_use1
     */
    public function setSignUse1($sign_use1): void
    {
        $this->sign_use1 = $sign_use1;
    }

    /**
     * @return mixed
     */
    public function getSignUse2()
    {
        return $this->sign_use2;
    }

    /**
     * @param mixed $sign_use2
     */
    public function setSignUse2($sign_use2): void
    {
        $this->sign_use2 = $sign_use2;
    }

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_plu;
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_use1;
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $sign_use2;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getOrbSol()
    {
        return $this->orb_sol;
    }

    /**
     * @param mixed $orb_sol
     */
    public function setOrbSol($orb_sol): void
    {
        $this->orb_sol = $orb_sol;
    }

    /**
     * @return mixed
     */
    public function getOrbLun()
    {
        return $this->orb_lun;
    }

    /**
     * @param mixed $orb_lun
     */
    public function setOrbLun($orb_lun): void
    {
        $this->orb_lun = $orb_lun;
    }

    /**
     * @return mixed
     */
    public function getOrbMer()
    {
        return $this->orb_mer;
    }

    /**
     * @param mixed $orb_mer
     */
    public function setOrbMer($orb_mer): void
    {
        $this->orb_mer = $orb_mer;
    }

    /**
     * @return mixed
     */
    public function getOrbWen()
    {
        return $this->orb_wen;
    }

    /**
     * @param mixed $orb_wen
     */
    public function setOrbWen($orb_wen): void
    {
        $this->orb_wen = $orb_wen;
    }

    /**
     * @return mixed
     */
    public function getOrbMar()
    {
        return $this->orb_mar;
    }

    /**
     * @param mixed $orb_mar
     */
    public function setOrbMar($orb_mar): void
    {
        $this->orb_mar = $orb_mar;
    }

    /**
     * @return mixed
     */
    public function getOrbJup()
    {
        return $this->orb_jup;
    }

    /**
     * @param mixed $orb_jup
     */
    public function setOrbJup($orb_jup): void
    {
        $this->orb_jup = $orb_jup;
    }

    /**
     * @return mixed
     */
    public function getOrbSat()
    {
        return $this->orb_sat;
    }

    /**
     * @param mixed $orb_sat
     */
    public function setOrbSat($orb_sat): void
    {
        $this->orb_sat = $orb_sat;
    }

    /**
     * @return mixed
     */
    public function getOrbUra()
    {
        return $this->orb_ura;
    }

    /**
     * @param mixed $orb_ura
     */
    public function setOrbUra($orb_ura): void
    {
        $this->orb_ura = $orb_ura;
    }

    /**
     * @return mixed
     */
    public function getOrbNep()
    {
        return $this->orb_nep;
    }

    /**
     * @param mixed $orb_nep
     */
    public function setOrbNep($orb_nep): void
    {
        $this->orb_nep = $orb_nep;
    }

    /**
     * @return mixed
     */
    public function getOrbPlu()
    {
        return $this->orb_plu;
    }

    /**
     * @param mixed $orb_plu
     */
    public function setOrbPlu($orb_plu): void
    {
        $this->orb_plu = $orb_plu;
    }

    /**
     * @return mixed
     */
    public function getSignSol()
    {
        return $this->sign_sol;
    }

    /**
     * @param mixed $sign_sol
     */
    public function setSignSol($sign_sol): void
    {
        $this->sign_sol = $sign_sol;
    }

    /**
     * @return mixed
     */
    public function getSignLun()
    {
        return $this->sign_lun;
    }

    /**
     * @param mixed $sign_lun
     */
    public function setSignLun($sign_lun): void
    {
        $this->sign_lun = $sign_lun;
    }

    /**
     * @return mixed
     */
    public function getSignMer()
    {
        return $this->sign_mer;
    }

    /**
     * @param mixed $sign_mer
     */
    public function setSignMer($sign_mer): void
    {
        $this->sign_mer = $sign_mer;
    }

    /**
     * @return mixed
     */
    public function getSignWen()
    {
        return $this->sign_wen;
    }

    /**
     * @param mixed $sign_wen
     */
    public function setSignWen($sign_wen): void
    {
        $this->sign_wen = $sign_wen;
    }

    /**
     * @return mixed
     */
    public function getSignMar()
    {
        return $this->sign_mar;
    }

    /**
     * @param mixed $sign_mar
     */
    public function setSignMar($sign_mar): void
    {
        $this->sign_mar = $sign_mar;
    }

    /**
     * @return mixed
     */
    public function getSignJup()
    {
        return $this->sign_jup;
    }

    /**
     * @param mixed $sign_jup
     */
    public function setSignJup($sign_jup): void
    {
        $this->sign_jup = $sign_jup;
    }

    /**
     * @return mixed
     */
    public function getSignSat()
    {
        return $this->sign_sat;
    }

    /**
     * @param mixed $sign_sat
     */
    public function setSignSat($sign_sat): void
    {
        $this->sign_sat = $sign_sat;
    }

    /**
     * @return mixed
     */
    public function getSignUra()
    {
        return $this->sign_ura;
    }

    /**
     * @param mixed $sign_ura
     */
    public function setSignUra($sign_ura): void
    {
        $this->sign_ura = $sign_ura;
    }

    /**
     * @return mixed
     */
    public function getSignNep()
    {
        return $this->sign_nep;
    }

    /**
     * @param mixed $sign_nep
     */
    public function setSignNep($sign_nep): void
    {
        $this->sign_nep = $sign_nep;
    }

    /**
     * @return mixed
     */
    public function getSignPlu()
    {
        return $this->sign_plu;
    }

    /**
     * @param mixed $sign_plu
     */
    public function setSignPlu($sign_plu): void
    {
        $this->sign_plu = $sign_plu;
    }















}