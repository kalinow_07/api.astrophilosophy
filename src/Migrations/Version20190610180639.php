<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190610180639 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE days_1');
        $this->addSql('DROP INDEX date ON days');
        $this->addSql('ALTER TABLE days DROP lil, CHANGE sol sol NUMERIC(10, 5) NOT NULL, CHANGE lun lun NUMERIC(10, 5) NOT NULL, CHANGE mer mer NUMERIC(10, 5) NOT NULL, CHANGE wen wen NUMERIC(10, 5) NOT NULL, CHANGE mar mar NUMERIC(10, 5) NOT NULL, CHANGE jup jup NUMERIC(10, 5) NOT NULL, CHANGE sat sat NUMERIC(10, 5) NOT NULL, CHANGE ura ura NUMERIC(10, 5) NOT NULL, CHANGE nep nep NUMERIC(10, 5) NOT NULL, CHANGE plu plu NUMERIC(10, 5) NOT NULL, CHANGE use1 use1 NUMERIC(10, 5) NOT NULL, CHANGE use2 use2 NUMERIC(10, 5) NOT NULL, CHANGE date date VARCHAR(12) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE days_1 (id INT AUTO_INCREMENT NOT NULL, sol NUMERIC(14, 5) DEFAULT NULL, lun NUMERIC(14, 5) DEFAULT NULL, mer NUMERIC(14, 5) DEFAULT NULL, wen NUMERIC(14, 5) DEFAULT NULL, mar NUMERIC(14, 5) DEFAULT NULL, jup NUMERIC(14, 5) DEFAULT NULL, sat NUMERIC(14, 5) DEFAULT NULL, ura NUMERIC(14, 5) DEFAULT NULL, nep NUMERIC(14, 5) DEFAULT NULL, plu NUMERIC(14, 5) DEFAULT NULL, use1 NUMERIC(14, 5) DEFAULT NULL, use2 NUMERIC(14, 5) DEFAULT NULL, lil NUMERIC(14, 5) DEFAULT NULL, date VARCHAR(10) NOT NULL COLLATE utf8_general_ci, INDEX date (date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE days ADD lil NUMERIC(14, 5) DEFAULT NULL, CHANGE sol sol NUMERIC(14, 5) DEFAULT NULL, CHANGE lun lun NUMERIC(14, 5) DEFAULT NULL, CHANGE mer mer NUMERIC(14, 5) DEFAULT NULL, CHANGE wen wen NUMERIC(14, 5) DEFAULT NULL, CHANGE mar mar NUMERIC(14, 5) DEFAULT NULL, CHANGE jup jup NUMERIC(14, 5) DEFAULT NULL, CHANGE sat sat NUMERIC(14, 5) DEFAULT NULL, CHANGE ura ura NUMERIC(14, 5) DEFAULT NULL, CHANGE nep nep NUMERIC(14, 5) DEFAULT NULL, CHANGE plu plu NUMERIC(14, 5) DEFAULT NULL, CHANGE use1 use1 NUMERIC(14, 5) DEFAULT NULL, CHANGE use2 use2 NUMERIC(14, 5) DEFAULT NULL, CHANGE date date VARCHAR(10) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('CREATE INDEX date ON days (date)');
    }
}
