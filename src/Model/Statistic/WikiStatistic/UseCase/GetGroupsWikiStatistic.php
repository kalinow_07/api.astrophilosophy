<?php


namespace App\Model\Statistic\WikiStatistic\UseCase;


use App\Model\Statistic\PlanetStatistic\UseCase\GetPlanetStatistic;
use App\Model\Statistic\Service\ChainCondition\ChainCondition;
use App\Model\Statistic\Service\ChainCondition\ChainConditionService;
use App\Repository\DayRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class GetGroupsWikiStatistic
{

    private $entityManager;
    private $chainConditionService;
    private $dayRepository;
    private $getPlanerStatistic;
    private $connection;

    private $days;

    public function __construct(EntityManagerInterface $entityManager,
                                ChainConditionService $chainConditionService, DayRepository $dayRepository,
                                GetPlanetStatistic $getPlanetStatistic)
    {

        $this->entityManager = $entityManager;
        $this->chainConditionService = $chainConditionService;
        $this->dayRepository = $dayRepository;
        $this->getPlanerStatistic = $getPlanetStatistic;
        $this->connection = $this->entityManager->getConnection();

    }

    public function get(ChainCondition $chainCondition, $param){

        // Дни по условиям
        $this->days = $this->getPlanerStatistic->get($chainCondition)["day_ids"];
        $days = '"' . implode('","', $this->days) . '"';

        if(isset($param["property"])){
            $property_where = 'people_property.property = "' . $param["property"] . '"';
        }

        // Поиск групп
        $sql = '
                SELECT groups_all.name_rus                         as value,
                       groups_all.name                            as value_eng,
                       groups_all.count                            as all_count,
                       groups_condition.count                      as count,
                       (groups_condition.count / groups_all.count) as percent,
                       groups_all.value_id as value_id
                FROM (
                         SELECT v.id as value_id, COUNT(v.id) as count, v.name_rus, v.name
                         FROM people_property
                                  LEFT JOIN peoples p on people_property.people_id = p.id
                                  LEFT JOIN value v on people_property.value_id = v.id
                         WHERE ' . $property_where . '
                           and v.name_rus != ""
                         GROUP BY v.id
                     ) as groups_all
                         LEFT JOIN (
                    SELECT v.id as value_id, COUNT(v.id) as count, v.name_rus
                    FROM people_property
                             LEFT JOIN peoples p on people_property.people_id = p.id
                             LEFT JOIN value v on people_property.value_id = v.id
                    WHERE ' . $property_where . '
                      and v.name_rus != ""
                      and p.birthday_date IN (' . $days . ')
                    GROUP BY v.id
                ) as groups_condition ON groups_all.value_id = groups_condition.value_id
                HAVING groups_condition.count > 5
                ORDER BY percent DESC LIMIT 500;
        ';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();

        return [
            "groups" => $stmt->fetchAll(),
            "param" => $param,
        ];
    }

    private function getConditions(){

        $conditions = [];

        //$conditions["days"] = implode(",", $this->days);

        return $conditions;
    }

}
