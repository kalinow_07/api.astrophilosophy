<?php

namespace App\Model\Wiki\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Model\Wiki\Repository")
 * @ORM\Table(name="category_values",indexes={@Index(name="category_idx", columns={"category_property_id"}),@Index(name="value_idx", columns={"value_id"})})
 */

class CategoryValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Wiki\Entity\CategoryProperty", inversedBy="category_properties")
     * @ORM\JoinColumn(name="category_property_id", referencedColumnName="id", onDelete="RESTRICT")
     */
    private $category_property_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Wiki\Entity\Value", inversedBy="values")
     * @ORM\JoinColumn(name="value_id", referencedColumnName="id", onDelete="RESTRICT")
     */
    private $value_id;


}