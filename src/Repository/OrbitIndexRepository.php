<?php

namespace App\Repository;

use App\Model\Chain\Entity\ChainType;
use App\Model\Chain\Entity\Day;
use App\Model\Chain\Entity\OrbitIndex;
use App\Model\Chain\Entity\Planet;
use App\Model\Chain\Entity\PlanetIndex;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Planet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planet[]    findAll()
 * @method Planet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrbitIndexRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrbitIndex::class);
    }

    // /**
    //  * @return Planet[] Returns an array of Planet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Planet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function getEm()
    {
        return $this->_em;
    }

    public function hasByPlanetAndDayAndChainType(Planet $planet, Day $day, ChainType $chainType){

            return (bool) $this->findOneBy(
                ['planet' => $planet, 'day' => $day, 'chain_type' => $chainType]
            );

    }
}
