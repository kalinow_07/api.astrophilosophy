<?php

namespace App\Model\FourWindow\UseCase\DoublePlanetWindow;

use App\Model\Chain\Entity\Planet;
use App\Model\FourWindow\Controller\PlanetController;
use App\Model\FourWindow\Service\ConvertPlanetName;
use App\Repository\PlanetRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DoublePlanetWindowForOrbit
{
    /**
     * @Assert\NotBlank
     */
    public $first_planet_id;
    /**
     * @Assert\NotBlank
     */
    public $second_planet_id;
    /**
     * @Assert\NotBlank
     */
    public $orbit;
    /**
     * @Assert\NotBlank
     */
    public $date_from;
    /**
     * @Assert\NotBlank
     */
    public $date_to;

    public $entityManager;
    public $validator;
    private $connection;
    private $planetRepository;
    private $convertPlanetName;
    private $first_column;
    private $second_column;

    public function __construct(EntityManagerInterface $entityManager,
                                ValidatorInterface $validator,PlanetRepository $planetRepository,
                                ConvertPlanetName $convertPlanetName)
    {

        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->connection = $this->entityManager->getConnection();
        $this->planetRepository = $planetRepository;
        $this->convertPlanetName = $convertPlanetName;


    }

    public function get(array $param){

        if(isset($param["first_planet_id"])){
            $this->first_planet_id = $param["first_planet_id"];
        }
        if(isset($param["second_planet_id"])){
            $this->second_planet_id = $param["second_planet_id"];
        }
        if(isset($param["orbit"])){
            $this->orbit = $param["orbit"];
        }
        if(isset($param["date_from"])){
            $this->date_from = $param["date_from"];
        }
        if(isset($param["date_to"])){
            $this->date_to = $param["date_to"];
        }

        $this->validation();

        $first_planet = $this->planetRepository->find($this->first_planet_id);
        if(!(bool)$first_planet) throw new Exception("First planet not find");

        $second_planet = $this->planetRepository->find($this->second_planet_id);
        if(!(bool)$second_planet) throw new Exception("Second planet not find");


        $first_column = "orb_" . $first_planet->getMarker();
        $second_column = "orb_" . $second_planet->getMarker();
        $this->first_column = $first_column;
        $this->second_column = $second_column;

        $sql = "
        SELECT $first_column, $second_column, COUNT(*) count FROM chains
        WHERE $first_column = $this->orbit AND date >= :date_from AND date <= :date_to
        GROUP BY $second_column
        LIMIT 20
        ";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            "date_from" => $this->date_from,
            "date_to" => $this->date_to,
        ]);

        return [
            "items" => $this->presenter($stmt->fetchAll())
        ];

    }

    private function validation(){
        $errors = $this->validator->validate($this);
        if(count($errors) > 0){
            throw new Exception($errors[0]->getMessage());
        }
    }


    private function presenter($items){

        $first_column = $this->first_column;
        $second_column = $this->second_column;
        $orbit = $this->orbit;
        foreach ($items as $key => $item){
            $items[$key]["name"] = $this->convertPlanetName->getPlanetName($first_column)
                . $this->convertPlanetName->getOrbitName($item[$first_column])
                . " + " . $this->convertPlanetName->getPlanetName($second_column)
                . $this->convertPlanetName->getOrbitName($item[$second_column]);
        }
        return $items;
    }

}