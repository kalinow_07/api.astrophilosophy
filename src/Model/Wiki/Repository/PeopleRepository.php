<?php

namespace App\Model\Wiki\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Model\Wiki\Entity\People;

/**
 * @method People|null find($id, $lockMode = null, $lockVersion = null)
 * @method People|null findOneBy(array $criteria, array $orderBy = null)
 * @method People[]    findAll()
 * @method People[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeopleRepository extends ServiceEntityRepository
{


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, People::class);
    }

     /**
      * @return People[] Returns an array of Chain objects
      */

    public function firstThen()
    {
        return $this->createQueryBuilder('d')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getAll(){

        return $this->createQueryBuilder('p')
            ->select("p.id")
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Chain
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function getEm()
    {
        return $this->_em;
    }
}
