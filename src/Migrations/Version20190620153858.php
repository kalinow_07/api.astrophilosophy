<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190620153858 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE planets DROP FOREIGN KEY FK_BDDA8B066FC7C15');
        $this->addSql('DROP INDEX IDX_BDDA8B066FC7C15 ON planets');
        $this->addSql('ALTER TABLE planets DROP sign_id');
        $this->addSql('ALTER TABLE signs ADD planet_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81A25E9820 FOREIGN KEY (planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_41946E81A25E9820 ON signs (planet_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE planets ADD sign_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE planets ADD CONSTRAINT FK_BDDA8B066FC7C15 FOREIGN KEY (sign_id) REFERENCES signs (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_BDDA8B066FC7C15 ON planets (sign_id)');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81A25E9820');
        $this->addSql('DROP INDEX IDX_41946E81A25E9820 ON signs');
        $this->addSql('ALTER TABLE signs DROP planet_id');
    }
}
