<?php
namespace App\Model\Chain\Service;

class Orbit
{

    public function all(){
        return [
            ["id" => 0, "name" => "В центре"],
            ["id" => 1, "name" => "Первая орбита"],
            ["id" => 2, "name" => "Вторая орбита"],
            ["id" => 3, "name" => "Третья орбита"],
            ["id" => 4, "name" => "Четвертая орбита"],
            ["id" => 5, "name" => "Пятая орбита"],
            ["id" => 6, "name" => "Шестая орбита"],
            ["id" => 7, "name" => "Седьмая орбита"],
            ["id" => 8, "name" => "Восьмая орбита"],
            ["id" => 9, "name" => "Девятая орбита"],
        ];
    }

}
