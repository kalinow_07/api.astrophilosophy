<?php

namespace App\Model\User\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\User\Entity\User;
use App\Model\User\Service\RegistrationPathService;
use App\Repository\PlanetRepository;
use App\Repository\UserRepository;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Chain\Presenter\Chain as ChainPresenter;

class UserController extends AbstractController
{


    /**
     * @Route("/get_registration_path", name="user_get_registration_path")
     */
    public function getRegistrationPath(RegistrationPathService $registrationPathService)
    {
        return $this->json([
            'path' => $registrationPathService->get(),
        ]);
    }


    /**
     * @Route("/user/get_by_code/{code}", name="user_get_by_code")
     */
    public function getByCode(UserRepository $userRepository, $code)
    {
        return $this->json($userRepository->getByCode($code));
    }
    /**
     * @Route("/user/get_by_user_id/{user_id}", name="user_get_by_user_id")
     */
    public function getByUserId(UserRepository $userRepository, $user_id)
    {
        return $this->json($userRepository->getUserByUserId((int)$user_id));
    }


    /**
     * @Route("/user/get_access_token", name="user_get_access_token")
     */
    public function getAccessToken(Request $request, UserRepository $userRepository)
    {

        if($_ENV['APP_ENV'] == "dev"){
//            return $this->json([
//                'result' => 1
//            ]);
            return $this->redirect('https://astroreader.ru');
        }

        if ($request->query->has('code')) {

            $code = $request->query->get('code');
            $state = $request->query->get('state');




            $params = $this->getParameter('user_registration');
            $client_id = $params["client_id"];
            $redirect_uri = $params["redirect_uri"];
            $client_secret = $params["client_secret"];
            $path = $params["vk_path"]
                . "?"
                . "client_id=$client_id&client_secret=$client_secret&redirect_uri=$redirect_uri&code=$code";

            $content_string = file_get_contents($path);
            $content = json_decode($content_string);


            $user = $userRepository->getUserByUserId($content->user_id);

            if (!(bool)$user) {
                $user = new User();
                $user->setUserId($content->user_id);
            }
            $user->setExpiresIn($content->expires_in);
            $user->setEmail($content->email);
            $user->setAccessToken($content->access_token);
            $user->setCode($state);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

//            return $this->json([
//                'result' => 1
//            ]);
            return $this->redirect('https://astroreader.ru');
        }
    }


}
