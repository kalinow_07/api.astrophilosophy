<?php

namespace App\Repository;

use App\Model\Chain\Entity\ChainType;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Model\Chain\Entity\Chain;

/**
 * @method ChainType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChainType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChainType[]    findAll()
 * @method ChainType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChainTypeRepository extends ServiceEntityRepository
{


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ChainType::class);
    }

    // /**
    //  * @return Chain[] Returns an array of Chain objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chain
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getByName($name){
        return $this->createQueryBuilder('s')
            ->andWhere('s.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getEm(){
        return $this->_em;
    }
}
