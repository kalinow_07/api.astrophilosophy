<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190624152201 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chains (id INT AUTO_INCREMENT NOT NULL, date VARCHAR(12) NOT NULL, orb_sol INT NOT NULL, orb_lun INT NOT NULL, orb_mer INT NOT NULL, orb_wen INT NOT NULL, orb_mar INT NOT NULL, orb_jup INT NOT NULL, orb_sat INT NOT NULL, orb_ura INT NOT NULL, orb_nep INT NOT NULL, orb_plu INT NOT NULL, sign_sol VARCHAR(5) NOT NULL, sign_lun VARCHAR(5) NOT NULL, sign_mer VARCHAR(5) NOT NULL, sign_wen VARCHAR(5) NOT NULL, sign_mar VARCHAR(5) NOT NULL, sign_jup VARCHAR(5) NOT NULL, sign_sat VARCHAR(5) NOT NULL, sign_ura VARCHAR(5) NOT NULL, sign_nep VARCHAR(5) NOT NULL, sign_plu VARCHAR(5) NOT NULL, INDEX date_idx (date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE chains');
    }
}
