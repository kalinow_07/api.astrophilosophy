<?php

namespace App\Model\Wiki\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Model\Wiki\Repository")
 * @ORM\Table(name="peoples",indexes={@Index(name="date_idx", columns={"birthday_date"})})
 */

class People
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
     */
    private $pageid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wikibase_item;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $column1;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $column2;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $column3;


    /**
     * @ORM\Column(type="string", length=255)
     */

    private $birthday;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date_of_death;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $height;

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthday_date;


    /**
     * @ORM\Column(type="text")
     */
    private $image;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPageid()
    {
        return $this->pageid;
    }

    /**
     * @param mixed $pageid
     */
    public function setPageid($pageid): void
    {
        $this->pageid = $pageid;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getWikibaseItem()
    {
        return $this->wikibase_item;
    }

    /**
     * @param mixed $wikibase_item
     */
    public function setWikibaseItem($wikibase_item): void
    {
        $this->wikibase_item = $wikibase_item;
    }

    /**
     * @return mixed
     */
    public function getColumn1()
    {
        return $this->column1;
    }

    /**
     * @param mixed $column1
     */
    public function setColumn1($column1): void
    {
        $this->column1 = $column1;
    }

    /**
     * @return mixed
     */
    public function getColumn2()
    {
        return $this->column2;
    }

    /**
     * @param mixed $column2
     */
    public function setColumn2($column2): void
    {
        $this->column2 = $column2;
    }

    /**
     * @return mixed
     */
    public function getColumn3()
    {
        return $this->column3;
    }

    /**
     * @param mixed $column3
     */
    public function setColumn3($column3): void
    {
        $this->column3 = $column3;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getDateOfDeath()
    {
        return $this->date_of_death;
    }

    /**
     * @param mixed $date_of_death
     */
    public function setDateOfDeath($date_of_death): void
    {
        $this->date_of_death = $date_of_death;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getBirthdayDate()
    {
        return $this->birthday_date;
    }

    /**
     * @param mixed $birthday_date
     */
    public function setBirthdayDate($birthday_date): void
    {
        $this->birthday_date = $birthday_date;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }



}
