<?php

namespace App\Repository;

use App\Model\Chain\Entity\Aspect;
use App\Model\Chain\Entity\Sign;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Aspect|null find($id, $lockMode = null, $lockVersion = null)
 * @method Aspect|null findOneBy(array $criteria, array $orderBy = null)
 * @method Aspect[]    findAll()
 * @method Aspect[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AspectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Aspect::class);
    }

    // /**
    //  * @return Sign[] Returns an array of Sign objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sign
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getByMarker($marker){
        return $this->createQueryBuilder('s')
            ->andWhere('s.marker = :marker')
            ->setParameter('marker', $marker)
            ->getQuery()
            ->getOneOrNullResult();
    }



}
