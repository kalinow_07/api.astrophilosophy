<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191116135352 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signs ADD wla_planet_id INT DEFAULT NULL, ADD ekz_planet_id INT DEFAULT NULL, ADD pad_planet_id INT DEFAULT NULL, ADD izg_planet_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E8178B90564 FOREIGN KEY (wla_planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E8155ADBCF5 FOREIGN KEY (ekz_planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81EA32AE90 FOREIGN KEY (pad_planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E813DC82D51 FOREIGN KEY (izg_planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_41946E8178B90564 ON signs (wla_planet_id)');
        $this->addSql('CREATE INDEX IDX_41946E8155ADBCF5 ON signs (ekz_planet_id)');
        $this->addSql('CREATE INDEX IDX_41946E81EA32AE90 ON signs (pad_planet_id)');
        $this->addSql('CREATE INDEX IDX_41946E813DC82D51 ON signs (izg_planet_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E8178B90564');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E8155ADBCF5');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81EA32AE90');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E813DC82D51');
        $this->addSql('DROP INDEX IDX_41946E8178B90564 ON signs');
        $this->addSql('DROP INDEX IDX_41946E8155ADBCF5 ON signs');
        $this->addSql('DROP INDEX IDX_41946E81EA32AE90 ON signs');
        $this->addSql('DROP INDEX IDX_41946E813DC82D51 ON signs');
        $this->addSql('ALTER TABLE signs DROP wla_planet_id, DROP ekz_planet_id, DROP pad_planet_id, DROP izg_planet_id');
    }
}
