<?php


namespace App\Model\Statistic\WikiStatistic\UseCase;


use App\Model\Statistic\PlanetStatistic\UseCase\GetPlanetStatistic;
use App\Model\Statistic\Service\ChainCondition\ChainCondition;
use App\Model\Statistic\Service\ChainCondition\ChainConditionService;
use App\Repository\DayRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class GetChartWikiStatistic
{

    private $entityManager;
    private $chainConditionService;
    private $dayRepository;
    private $getPlanetStatistic;
    private $connection;

    private $days;

    public function __construct(EntityManagerInterface $entityManager,
                                ChainConditionService $chainConditionService, DayRepository $dayRepository,
                                GetPlanetStatistic $getPlanetStatistic)
    {

        $this->entityManager = $entityManager;
        $this->chainConditionService = $chainConditionService;
        $this->dayRepository = $dayRepository;
        $this->getPlanetStatistic = $getPlanetStatistic;
        $this->connection = $this->entityManager->getConnection();

    }

    public function get(ChainCondition $chainCondition, $param){

        // Дни по условиям
        $this->days = $this->getPlanetStatistic->get($chainCondition)["day_ids"];
        $days = '"' . implode('","', $this->days) . '"';
        $groups = '"' . implode('","', json_decode($param["group_ids"])) . '"';

        // Поиск групп
        $sql = '
            SELECT pp1.value_id as value_id_1, pp2.value_id as value_id_2, v1.name_rus as value_1, v2.name_rus as value_2, COUNT(*) as count
            FROM people_property as pp1
                     JOIN people_property as pp2 ON pp1.people_id = pp2.people_id
                     LEFT JOIN peoples p1 on pp1.people_id = p1.id
                     LEFT JOIN peoples p2 on pp2.people_id = p2.id
                     LEFT JOIN value v1 on pp1.value_id = v1.id
                     LEFT JOIN value v2 on pp2.value_id = v2.id
            WHERE pp1.value_id IN (' . $groups . ') AND pp2.property = "' . $param["property"] . '" 
              and pp1.id != pp2.id
              and p1.birthday_date IN
                  (' . $days . ')
            GROUP BY pp1.value_id, pp2.value_id
            HAVING count > 3
            ORDER BY count DESC
            LIMIT 100000
        ';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();


        $result = $stmt->fetchAll();

        return [
            "data" => $this->formatResult($result),
            "param" => $param,
            "items" => $result,
        ];
    }

    private function getConditions(){

        $conditions = [];

        //$conditions["days"] = implode(",", $this->days);

        return $conditions;
    }

    private function formatResult($data){
        $nodes = [];
        $links = [];
        $keys = [];
        $node_keys = [];
        foreach ($data as $item){



            if(!in_array($item["value_id_1"], $node_keys)){
                $node_keys[] = $item["value_id_1"];
                $nodes[] = ["id" => $item["value_1"], "group" => 1];
            }
            if(!in_array($item["value_id_2"], $node_keys)){
                $node_keys[] = $item["value_id_2"];
                $nodes[] = ["id" => $item["value_2"], "group" => 1];
            }

            $key = $item["value_id_1"] . "_" . $item["value_id_2"];
            $key_reverse = $item["value_id_2"] . "_" . $item["value_id_1"];

            if(!in_array($key, $keys) && !in_array($key_reverse, $keys)){
                $keys[] = $key;
                $links[] = ["source" => $item["value_1"], "target" => $item["value_2"], "value" => $item["count"]];
            }

        }

        return ["links" => $links, "nodes" => $nodes];
    }

}
