<?php

namespace App\Model\User\Service;


use App\Repository\PlanetRepository;
use App\Repository\SignRepository;

class RegistrationPathService
{

    private $userRegistrationParam;

    public function __construct($userRegistrationParam)
    {
        $this->userRegistrationParam = $userRegistrationParam;

    }

    public function get()
    {

        $vk_path_authorize = $this->userRegistrationParam["vk_path_authorize"];
        $client_id = $this->userRegistrationParam["client_id"];
        $redirect_uri = $this->userRegistrationParam["redirect_uri"];
        $display = $this->userRegistrationParam["display"];
        $scope = $this->userRegistrationParam["scope"];
        $response_type = $this->userRegistrationParam["response_type"];
        $v = $this->userRegistrationParam["v"];

        return "$vk_path_authorize?client_id=$client_id&display=$display&redirect_uri=$redirect_uri&scope=$scope&response_type=$response_type&v=$v";

    }


}
