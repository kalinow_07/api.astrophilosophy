<?php


namespace App\Model\Statistic\PlanetStatistic\UseCase;


use App\Model\Statistic\Service\ChainCondition\ChainCondition;
use App\Model\Statistic\Service\ChainCondition\ChainConditionService;
use App\Repository\DayRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class GetPlanetStatistic
{

    private $entityManager;
    private $chainConditionService;
    private $dayRepository;

    public function __construct(EntityManagerInterface $entityManager,
                                ChainConditionService $chainConditionService, DayRepository $dayRepository)
    {

        $this->entityManager = $entityManager;
        $this->chainConditionService = $chainConditionService;
        $this->dayRepository = $dayRepository;

    }

    public function get(ChainCondition $chainCondition){
        try{

            $condition_by_date = $this->chainConditionService->getConditionByDate($chainCondition);

            $em = $this->entityManager;
            $expr = $this->entityManager->getExpressionBuilder();
            $qb = $em->createQueryBuilder();

            $qb->select('d.id', 'd.date')
                ->from('Chain:Day', 'd');

            foreach ($condition_by_date["planets"] as $key => $planet){
                $alias = "_" . $key;
                $qb->andWhere(
                    $expr->in(
                        'd.id',
                        $this->getSubQuery($em, $alias, $condition_by_date, $planet)
                    )
                );
                $params["orbit$alias"] = $planet["orbit"];
                $params["planet_id$alias"] = $planet["id"];
                if((bool)$condition_by_date["by_sign"]) $params["sign_id$alias"] = $planet["sign_id"];
//                if((bool)$condition_by_date["by_aspect"])
//                foreach ($planet["aspects"] as $aspect){
//                    $aspect_alias = "aspect_id_$alias" . "_" . $aspect["aspect"];
//                    $params["$aspect_alias"] = $aspect["id"];
//                }
            }

            if((bool)$condition_by_date["date_from"]) $qb->andWhere($qb->expr()->gte('d.date_format', ':date_from'));
            if((bool)$condition_by_date["date_to"]) $qb->andWhere($qb->expr()->lte('d.date_format', ':date_to'));

            $params["chain_type_id"] = 1;
            if((bool)$condition_by_date["date_from"]) $params["date_from"] = $condition_by_date["date_from"];
            if((bool)$condition_by_date["date_to"]) $params["date_to"] = $condition_by_date["date_to"];

            $qb->setParameters($params)
                ->groupBy('d.id')
                ->setMaxResults(1000000);

            $query = $qb->getQuery();
            $result = $query->getResult();

            return [
                "count_condition" => count($result),
                "count_all" => $this->dayRepository->getCountByRange($condition_by_date["date_from"], $condition_by_date["date_to"]),
                "date_from" => $condition_by_date["date_from"],
                "date_to" => $condition_by_date["date_to"],
                "chain" => $condition_by_date["chain"],
                "condition_by_date" => $condition_by_date,
//                "days" => array_slice($result, 0, 10000),
                "days" => $this->formatDate(array_slice($result, 0, 100000)),
                "message" => $condition_by_date["message"],
                "day_ids" => $this->getDateIdsFromResult($result)
            ];
            return $this->chainConditionService->getConditionByDate($chainCondition);
            return $chain_condition;

        }catch (Exception $exception){
            return $exception->getMessage();
        }

    }

    private function getSubQuery($em, $alias, $condition_by_date, $planet){
            $em = $em->createQueryBuilder();
            $em = $em->select("identity(oi$alias.day)");
            $em = $em->from("Chain:OrbitIndex", "oi$alias");
            $em = $em->join("Chain:PlanetIndex", "pi$alias", "WITH", "oi$alias.day = pi$alias.day");
            $em = $em->join("Chain:AspectIndex", "ai$alias", "WITH", "oi$alias.day = ai$alias.day");
            $em = $em->where("pi$alias.planet = :planet_id$alias");
            $em = $em->andWhere("oi$alias.planet = :planet_id$alias");
            if((bool)$condition_by_date["by_sign"]) $em = $em->andWhere("pi$alias.sign = :sign_id$alias");
//            if((bool)$condition_by_date["by_aspect"])
//            foreach ($planet["aspects"] as $aspect){
//                $aspect_alias = "aspect_id_$alias" . "_" . $aspect["aspect"];
//                $em = $em->andWhere("ai$alias.aspect = :$aspect_alias");
//            }
            $em = $em->andWhere("oi$alias.chain_type = :chain_type_id");
            $em = $em->andWhere("oi$alias.orbit = :orbit$alias");
            $em = $em->groupBy("oi$alias.day");
            $em = $em->getDQL();
            return $em;
    }

    private function formatDate($dates){

        $result = [];

        foreach ($dates as $key => $date){
            $year = Carbon::parse($date["date"])->format('Y');
            if(array_key_exists($year, $result)){
                $result[$year][] = Carbon::parse($date["date"])->format('Y-m-d');
            }else{
                $result[$year] = [];
                $result[$year][] = Carbon::parse($date["date"])->format('Y-m-d');
            }
        }

        $post_result = [];
        foreach ($result as $key => $item){
            $post_result[] = [
                "year" => $key,
                "items" => $item
            ];
        }
        return $post_result;
    }

    private function getDateIdsFromResult($dates){
        $result = [];
        foreach ($dates as $date){
            $result[] = Carbon::parse($date["date"])->format('Y-m-d');
        }
        return $result;
    }

}
