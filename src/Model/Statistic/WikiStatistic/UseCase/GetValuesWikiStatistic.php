<?php


namespace App\Model\Statistic\WikiStatistic\UseCase;


use App\Model\Statistic\PlanetStatistic\UseCase\GetPlanetStatistic;
use App\Model\Statistic\Service\ChainCondition\ChainCondition;
use App\Model\Statistic\Service\ChainCondition\ChainConditionService;
use App\Repository\DayRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class GetValuesWikiStatistic
{

    private $chainConditionService;
    private $dayRepository;
    private $getPlanetStatistic;
    private $entityManager;
    private $connection;

    private $days;

    public function __construct(EntityManagerInterface $entityManager,
                                ChainConditionService $chainConditionService, DayRepository $dayRepository,
                                GetPlanetStatistic $getPlanetStatistic)
    {

        $this->chainConditionService = $chainConditionService;
        $this->dayRepository = $dayRepository;
        $this->getPlanetStatistic = $getPlanetStatistic;
        $this->entityManager = $entityManager;
        $this->connection = $this->entityManager->getConnection();

    }

    public function get(ChainCondition $chainCondition, $param){

        // Дни по условиям
        $this->days = $this->getPlanetStatistic->get($chainCondition)["day_ids"];
        $days = '"' . implode('","', $this->days) . '"';
        $groups = '"' . implode('","', json_decode($param["group_ids"])) . '"';
        $count_limit = count(json_decode($param["group_ids"]));

        // Поиск групп
        $sql = '
             SELECT p.*, COUNT(*) count FROM peoples as p
                JOIN people_property pp on p.id = pp.people_id
                WHERE p.birthday_date IN (' . $days . ')
                AND pp.value_id IN (' .$groups . ')
            GROUP BY p.id
            HAVING count = ' . $count_limit . '
            LIMIT 100;
        ';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();

        return [
            "people" => $stmt->fetchAll(),
            "param" => $param,
        ];
    }

    private function getConditions(){

        $conditions = [];

        //$conditions["days"] = implode(",", $this->days);

        return $conditions;
    }

}
