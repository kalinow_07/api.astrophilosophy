<?php

namespace App\Repository;

use App\Model\Compatibility\Entity\Compatibility;
use App\Model\Chain\Entity\Planet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Compatibility|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compatibility|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compatibility[]    findAll()
 * @method Compatibility[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompatibilityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Compatibility::class);
    }

    // /**
    //  * @return Planet[] Returns an array of Planet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Planet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */



    public function getAllWithOrderBySignLun()
    {
        $sql = '
        select date, sign_lun, compatibilities.birthday_date1, compatibilities.birthday_date2 from chains join compatibilities on date = compatibilities.birthday_date1 
        
          where wikibase_item1 NOT IN (SELECT wikibase_item2 from compatibilities as c2 group by c2.wikibase_item2) order by FIELD(sign_lun,
                                                                                                               "owe",
                                                                                                               "tel",
                                                                                                               "bli",
                                                                                                               "rak",
                                                                                                               "lew",
                                                                                                               "dew",
                                                                                                               "wes",
                                                                                                               "sko",
                                                                                                               "str",
                                                                                                               "koz",
                                                                                                               "wod",
                                                                                                               "ryb")
                                                                                                               
              ';
        $em = $this->_em;
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }



    public function getAllWithOrderBySignSol()
    {
        $sql = '
        select date, sign_sol, compatibilities.birthday_date1, compatibilities.birthday_date2 from chains join compatibilities on date = compatibilities.birthday_date1 
        
          where wikibase_item1 NOT IN (SELECT wikibase_item2 from compatibilities as c2 group by c2.wikibase_item2) order by FIELD(sign_sol,
    "owe",
    "tel",
    "bli",
    "rak",
    "lew",
    "dew",
    "wes",
    "sko",
    "str",
    "koz",
    "wod",
    "ryb")
                                                                                                               
              ';
        $em = $this->_em;
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

}
