<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190619173224 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE planets CHANGE sign_id sign_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE planets ADD CONSTRAINT FK_BDDA8B066FC7C15 FOREIGN KEY (sign_id) REFERENCES signs (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_BDDA8B066FC7C15 ON planets (sign_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE planets DROP FOREIGN KEY FK_BDDA8B066FC7C15');
        $this->addSql('DROP INDEX IDX_BDDA8B066FC7C15 ON planets');
        $this->addSql('ALTER TABLE planets CHANGE sign_id sign_id INT NOT NULL');
    }
}
