<?php

namespace App\Model\Chain\UseCase;

use App\Repository\AspectRepository;
use App\Repository\DayRepository;
use App\Repository\PlanetRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Chain
{

    public $date;
    // Список платен цепочки
    private $planets = [];
    private $planetRepository;
    private $dayRepository;
    private $signRepository;
    private $planet;
    public $count_on_orbit;
    private $day;
    private $logger;
    private $type;
    private $aspects = [];
    private $aspectRepository;
    private $aspect;
    private $bal;

    public function __construct(PlanetRepository $planetRepository,
                                DayRepository $dayRepository,
                                Planet $planet,
                                SignRepository $signRepository,
                                Day $day,
                                LoggerInterface $logger,
                                AspectRepository $aspectRepository,
                                Aspect $aspect, Bal $bal)
    {
        $this->planetRepository = $planetRepository;
        $this->dayRepository = $dayRepository;
        $this->signRepository = $signRepository;
        $this->planet = $planet;
        $this->day = $day;
        $this->logger = $logger;
        $this->aspectRepository = $aspectRepository;
        $this->aspect = $aspect;
        $this->bal = $bal;
    }

    public function build(Carbon $date, $type = "wla")
    {



        $this->date = $date;
        $this->type = $type;

        // Создание объектов планет
        $this->initPlanet();

        // Определение градуса
        $this->setDegree();

        // Определение знака
        $this->setSign();

        // Определение родителя
        $this->setParent();

        // Определение потомков
        $this->getChildren();

        // Определение ретроградности
        $this->getRetrograde();

        // Определение орбиты
        $this->getOrbit();

        // Количество планет по орбитам
        $this->getCountOnOrbit();

        // Аспекты
        $this->getAspect();

        // Скорость
        $this->getSpeed();

        // Скорость
        $this->getBal();

        return [
            "planets" => $this->planets,
            "aspects" => $this->aspects,
        ];

    }

    private function initPlanet()
    {

        $this->planets = [];

        $planets = $this->planetRepository->findAll();

        foreach ($planets as $key => $planet) {
            $this->planets[] = $planet;
        }

    }

    private function setDegree()
    {

        foreach ($this->planets as $planet) {
            $sub_date = $this->date->copy()->subDay();
            $day = $this->day->get($this->date, $sub_date);
            $planet->degree = $day->getDegree($planet);
            $day_yesterday = $this->day->get($this->date->copy()->subDay(), $sub_date->copy()->subDay());
            $planet->degree_yesterday = $day_yesterday->getDegree($planet);
        }


    }

    private function setSign()
    {
        foreach ($this->planets as $planet) {
            $sign = $this->planet->getSign($planet->degree);
            $planet->sign = $sign->getMarker();
            $planet->sign_name = $sign->getName();
            $planet->sign_id = $sign->getId();
        }
    }

    private function setParent()
    {
        foreach ($this->planets as $planet) {
            $planet->parent = $this->planet->getParent($planet->degree, $this->type);
        }
    }

    private function getChildren()
    {

        foreach ($this->planets as $planet) {
            $children = [];
            foreach ($this->planets as $planet_child) {
                if ($planet->marker == $planet_child->marker) {
                    $children[] = $planet_child->marker;
                }
            }
            $planet->children = $children;
        }

    }

    private function getRetrograde()
    {
        foreach ($this->planets as $planet) {
            $result = null;
            $different = abs((float)$planet->degree - (float)$planet->degree_yesterday);
            if ((float)$planet->degree > (float)$planet->degree_yesterday) {
                $result = false;
            } else {
                $result = true;
            }
            if ($different > 100) {
                $result = !$result;
            }
            $planet->retrograde = $result;
        }
    }

    private function getOrbit()
    {

        $orbit = new Orbit($this->planets);
        foreach ($this->planets as $planet) {
            $planet->orbit = $orbit->get($planet);
        }

        $center = new Center($this->planets);
        foreach ($this->planets as $planet) {
            $planet->center = $center->get($planet);
        }

    }

    private function getCountOnOrbit()
    {
        $orbits = [];
        foreach ($this->planets as $planet) {
            $orbits[$planet->marker] = $planet->orbit;
        }

        $this->count_on_orbit = array_count_values($orbits);
    }

    private function getAspect(){
        $this->aspects = [];
        foreach ($this->planets as $planet1) {
            foreach ($this->planets as $planet2) {
                if($planet1 === $planet2) continue;
                $aspect = $this->aspect->getAspect($planet1, $planet2);
                if((bool)$aspect){
                    $this->aspects[] = $aspect;
                }
            }
        }

    }

    private function getSpeed(){

        foreach ($this->planets as $planet) {
            $speed = abs($planet->degree - $planet->degree_yesterday);
            $planet->speed = (100 * $speed) / $planet->avg_speed;
        }

    }

    private function getBal(){

        foreach ($this->planets as $planet) {
            $planet->bal = $this->bal->get($planet);
        }

    }

}
