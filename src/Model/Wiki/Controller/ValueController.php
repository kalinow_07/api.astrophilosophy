<?php

namespace App\Model\Wiki\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\FourWindow\UseCase\EventWindow\EventWindow;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForOrbit;
use App\Model\Wiki\Entity\CategoryProperty;
use App\Model\Wiki\Entity\Value;
use App\Repository\PlanetRepository;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ValueController extends AbstractController
{

    /**
     * @Route("/wiki/value", name="wiki_value_all", methods={"GET"})
     */
    public function all()
    {
        try{


            $result = $this->getDoctrine()
                ->getRepository(Value::class)
                ->find(36);

            $response = new JsonResponse($result);
            //$response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }

//    /**
//     * @Route("/wiki/category_property", name="wiki_category_property_post", methods={"POST"})
//     */
//    public function post(Request $request)
//    {
//        try{
//
//            $entityManager = $this->getDoctrine()->getManager();
//
//            $name = $request->request->get("name");
//
//            $categoryProperty = new CategoryProperty();
//            $categoryProperty->setName($name);
//
//            $entityManager->persist($categoryProperty);
//            $entityManager->flush();
//
//            $response = new JsonResponse(["categoryProperty" => 123]);
//            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
//
//            return $response;
//
//        }catch (Exception $exception){
//            return $this->json(["error" => $exception->getMessage()]);
//        }
//    }
//
//    /**
//     * @Route("/wiki/category_property/{id}", name="wiki_category_property_put", requirements={"id"="\d+"}, methods={"PUT"})
//     */
//    public function put($id, Request $request)
//    {
//        try{
//
//
//            $result = ["name" => "name1"];
//
//            $response = new JsonResponse($result);
//            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
//
//            return $response;
//
//        }catch (Exception $exception){
//            return $this->json(["error" => $exception->getMessage()]);
//        }
//    }
}
