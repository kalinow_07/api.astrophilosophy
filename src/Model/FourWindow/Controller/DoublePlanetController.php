<?php

namespace App\Model\FourWindow\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\FourWindow\UseCase\DoublePlanetWindow\DoublePlanetWindowForOrbit;
use App\Model\FourWindow\UseCase\DoublePlanetWindow\DoublePlanetWindowForSign;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForOrbit;
use App\Repository\PlanetRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DoublePlanetController extends AbstractController
{
    /**
     * @Route("/four_window/double_planet/get_by_orbit", name="four_window_double_planet_get_by_orbit")
     */
    public function getByOrbit(Request $request, DoublePlanetWindowForOrbit $doublePlanetWindow)
    {

        try{

            $param = $request->query->all();
            $result = $doublePlanetWindow->get($param);
            $response = new JsonResponse($result);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
            return $response;
        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/four_window/double_planet/get_by_sign", name="four_window_double_planet_get_by_sign")
     */
    public function index(Request $request, DoublePlanetWindowForSign $planetWindowForSign)
    {

        try{

            $param = $request->query->all();
            $result = $planetWindowForSign->get($param);
            $response = new JsonResponse($result);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }
}
