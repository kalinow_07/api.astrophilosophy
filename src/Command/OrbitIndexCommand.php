<?php

namespace App\Command;

use App\Model\Chain\Entity\OrbitIndex;
use App\Model\Chain\Entity\PlanetIndex;
use App\Model\Chain\UseCase\Chain;
use App\Model\Chain\Entity\Chain as ChainModel;
use App\Repository\ChainRepository;
use App\Repository\ChainTypeRepository;
use App\Repository\DayRepository;
use App\Repository\OrbitIndexRepository;
use App\Repository\PlanetIndexRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class OrbitIndexCommand extends Command
{

    private $chainRepository;
    private $chain;
    private $dayRepository;
    private $planetIndexRepository;
    private $orbitIndexRepository;
    private $signRepository;
    private $chainTypeRepository;

    public function __construct(ChainRepository $chainRepository,
                                Chain $chain,
                                DayRepository $dayRepository,
                                PlanetIndexRepository $planetIndexRepository,
                                SignRepository $signRepository,
                                ChainTypeRepository $chainTypeRepository,
                                OrbitIndexRepository $orbitIndexRepository)
    {
        $this->chain = $chain;
        $this->chainRepository = $chainRepository;
        $this->dayRepository = $dayRepository;
        $this->planetIndexRepository = $planetIndexRepository;
        $this->signRepository = $signRepository;
        $this->chainTypeRepository = $chainTypeRepository;
        $this->orbitIndexRepository = $orbitIndexRepository;
        parent::__construct();
    }


    protected static $defaultName = 'orbit_index';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('date', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $console_start = $output->section();
        $console_progress = $output->section();
        $console_finish = $output->section();

        $this->orbitIndexRepository->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->dayRepository->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->chainTypeRepository->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);


        $console_start->writeln('Начало заполнения таблицы orbit_indexes');

        $days = $this->dayRepository->getAllDates();

        $count = count($days);

        foreach ($days as $key => $date){

            $input_date = Carbon::parse($input->getArgument('date'))->timestamp;

            if(Carbon::parse($date["date"])->timestamp < $input_date) continue;

            $console_progress->overwrite(round($key * 100 / $count, 2) . "/" . $date["date"]);

            $day = $this->dayRepository->getDayByDate(Carbon::parse($date["date"]));

            foreach ($this->chainTypeRepository->findAll() as $chainType){

                $chain = $this->chain->build(Carbon::parse($day->getDate()), $chainType->getName());

                foreach ($chain["planets"] as $planet){

                    if($this->orbitIndexRepository->hasByPlanetAndDayAndChainType($planet, $day, $chainType)) continue;

                    $orbit_index = new OrbitIndex();
                    $orbit_index->setPlanet($planet);
                    $orbit_index->setDay($day);
                    $orbit_index->setChainType($chainType);
                    $orbit_index->setOrbit($planet->orbit);
                    $orbit_index->setBal($planet->bal);
                    $this->orbitIndexRepository->getEm()->persist($orbit_index);
                }

            }

            if (($key % 10) == 0){
                $this->orbitIndexRepository->getEm()->flush();
                $this->orbitIndexRepository->getEm()->clear();
            }

        }

        $this->orbitIndexRepository->getEm()->flush();

        $console_finish->writeln('Готово!');

    }
}
