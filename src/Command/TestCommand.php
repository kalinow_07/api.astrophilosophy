<?php

namespace App\Command;

use App\Model\Chain\UseCase\Chain;
use App\Model\Chain\UseCase\Day;
use App\Model\Compatibility\UseCase\Compatibility;
use App\Repository\CompatibilityRepository;
use App\Repository\DayRepository;
use App\Repository\PlanetRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Tests\Models\Taxi\Car;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TestCommand extends Command
{
    protected static $defaultName = 'TestCommand';

    private $chain;
    private $signRepository;
    private $compatibility;
    private $compatibilityRepository;
    private $dayRepository;
    private $planetRepository;
    private $logger;
    private $entityManager;

    private $unic_confirm = [];

    public function __construct(Chain $chain,
                                SignRepository $signRepository,
                                Compatibility $compatibility,
                                CompatibilityRepository $compatibilityRepository,
                                DayRepository $dayRepository,
                                PlanetRepository $planetRepository,
                                LoggerInterface $logger, EntityManagerInterface $entityManager)
    {
        $this->chain = $chain;
        $this->signRepository = $signRepository;
        $this->compatibility = $compatibility;
        $this->compatibilityRepository = $compatibilityRepository;
        $this->dayRepository = $dayRepository;
        $this->planetRepository = $planetRepository;
        $this->logger = $logger;
        $this->entityManager = $entityManager;


        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


//        $io = new SymfonyStyle($input, $output);
//        $arg1 = $input->getArgument('arg1');
//
//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }
//
//        if ($input->getOption('option1')) {
//            // ...
//        }
//


        $days = $this->dayRepository->getAllDates();
        $section1 = $output->section();
        $section1->writeln(count($days));

        return true;


        $planets = [
            "sol" => [
                "id" => 1,
                "orbit" => 3,
            ],
            "lun" => [
                "id" => 2,
                "orbit" => 3,
            ],
            "mer" => [
                "id" => 3,
                "orbit" => 3,
            ],
        ];
        $params = [];

        $em = $this->entityManager;
        $expr = $this->entityManager->getExpressionBuilder();
        $qb = $em->createQueryBuilder();

        $qb->select('d.id', 'd.date')
            ->from('Chain:Day', 'd');

        foreach ($planets as $key => $planet){
            $alias = "_" . $key;
            $qb->andWhere(
                $expr->in(
                    'd.id',
                    $em->createQueryBuilder()
                        ->select("identity(oi$alias.day)")
                        ->from("Chain:OrbitIndex", "oi$alias")
                        ->join("Chain:PlanetIndex", "pi$alias", "WITH", "oi$alias.day = pi$alias.day")
                        ->leftJoin("Chain:AspectIndex", "ai$alias", "WITH", "oi$alias.day = ai$alias.day")
                        ->where("pi$alias.planet = :planet_id$alias")
                        ->andWhere("oi$alias.planet = :planet_id$alias")
                        ->andWhere("oi$alias.chain_type = :chain_type_id")
                        ->andWhere("oi$alias.orbit = :orbit$alias")
                        ->groupBy("oi$alias.day")
                        ->getDQL()
                )
            );
            $params["planet_id$alias"] = $planet["id"];
            $params["orbit$alias"] = $planet["orbit"];
        }
        $params["chain_type_id"] = 1;

            $qb->setParameters($params)
            ->groupBy('d.id')
            ->setMaxResults(1000000);


//        $query = $qb->getQuery();
//        $result = $query->getDQL();
//
//        $section1 = $output->section();
//        $section1->writeln($result);
//        return true;

        $query = $qb->getQuery();
        $result = $query->getResult();

        $section1 = $output->section();
        $section1->writeln(count($result));
        $section1->writeln(json_encode($result));

        return true;


        $section1 = $output->section();
        $result = $this->chain->build(Carbon::parse("01-01-2019 16:15:00"), "wla");
        $section1->writeln(json_encode($result));
        $section1->writeln("==================================");

        return true;

//        $date_first = Carbon::now()->subYear()->subMonths(8);
//        $date_second = Carbon::now();
//        $compatibility = $this->compatibility->build($date_first, $date_second);
//        $section1 = $output->section();
//
//        $section1->writeln(json_encode($compatibility));




//        // Подготовка массива. Убрать повторяющиеся значения
//        $section1 = $output->section();
//        //$day = $this->dayRepository->getDayByDate(Carbon::parse("20-05-1986"));
//        $planet = $this->planetRepository->find(2);
//        $dayCase = new Day($this->dayRepository);
//
//        $day = $dayCase->get(Carbon::parse("30-01-1986 23:00:00"));
//        $result = $day->getDegree($planet);
//
//        $section1->overwrite($result);

        return true;

        $unic_all = [];




        $all = $this->compatibilityRepository->findAll();
        foreach ($all as $item){
            if($this->isConfirm($item->getWikibaseItem1(), $item->getWikibaseItem2()) === false){
                $unic_all[] = $item;
                $this->unic_confirm[] = ["wiki_item1" => $item->getWikibaseItem1(), "wiki_item2" => $item->getWikibaseItem2()];
            }
        }


        $section1 = $output->section();
        $section2 = $output->section();

        //$compatibilities = $this->compatibilityRepository->getAllWithOrderBySignSol();

        $sum_real = 0;
        $sum_fictive = 0;

        foreach ($unic_all as $key => $compatibility){
            $section1->overwrite($key);
            //$section1->writeln($compatibility->getBirthdayDate1());
            $compatibility_real = $this->compatibility->build(Carbon::parse($compatibility->getBirthdayDate1()), Carbon::parse($compatibility->getBirthdayDate2()));
            //$sum_real = $sum_real + $compatibility_real["rating"]["sum"];
            //$section1->overwrite("По звездам: " . $sum_real);

            $date_fictive1 = Carbon::now()->subYears(20)->subDays(rand(1, (365*100)));
            $date_fictive2 =  Carbon::parse($date_fictive1->format("Y-m-d"))->subDays(rand(1, (365*5)));
            $compatibility_fictive = $this->compatibility->build($date_fictive1, $date_fictive2);
            //$sum_fictive = $sum_fictive + $compatibility_fictive["rating"]["sum"];

            $fp = fopen('file.csv', 'a+');
            fputcsv($fp, [
                $compatibility->getBirthdayDate1(),
                $compatibility->getBirthdayDate2(),
                $compatibility_real["count_planets"]["give"],
                $compatibility_real["count_planets"]["private_inner"],
                $compatibility_real["count_planets"]["social_inner"],
                $compatibility_real["count_planets"]["private_outer"],
                $compatibility_real["count_planets"]["social_outer"],
                $date_fictive1,
                $date_fictive2,
                $compatibility_fictive["count_planets"]["give"],
                $compatibility_fictive["count_planets"]["private_inner"],
                $compatibility_fictive["count_planets"]["social_inner"],
                $compatibility_fictive["count_planets"]["private_outer"],
                $compatibility_fictive["count_planets"]["social_outer"],
            ]);
            fclose($fp);

            //$section2->overwrite("По звездам: " . $sum_real . "   <<<<>>>>   Случайные: " . $sum_fictive);
            //$section2->overwrite($date_fictive1->format("Y-m-d") . "   <<<<>>>>   Случайные: " . $date_fictive2->format("Y-m-d"));

        }




    }

    private function isConfirm($wiki_item1, $wiki_item2){


        foreach ($this->unic_confirm as $item){

            if($item["wiki_item1"] == $wiki_item2 && $item["wiki_item2"] == $wiki_item1 ){
                return true;
            }
        }
        return false;
    }
}
