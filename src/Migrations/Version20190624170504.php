<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190624170504 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chains CHANGE date date VARCHAR(12) DEFAULT NULL, CHANGE orb_lun orb_lun INT DEFAULT NULL, CHANGE orb_mer orb_mer INT DEFAULT NULL, CHANGE orb_wen orb_wen INT DEFAULT NULL, CHANGE orb_mar orb_mar INT DEFAULT NULL, CHANGE orb_jup orb_jup INT DEFAULT NULL, CHANGE orb_sat orb_sat INT DEFAULT NULL, CHANGE orb_ura orb_ura INT DEFAULT NULL, CHANGE orb_nep orb_nep INT DEFAULT NULL, CHANGE orb_plu orb_plu INT DEFAULT NULL, CHANGE sign_sol sign_sol VARCHAR(5) DEFAULT NULL, CHANGE sign_lun sign_lun VARCHAR(5) DEFAULT NULL, CHANGE sign_mer sign_mer VARCHAR(5) DEFAULT NULL, CHANGE sign_wen sign_wen VARCHAR(5) DEFAULT NULL, CHANGE sign_mar sign_mar VARCHAR(5) DEFAULT NULL, CHANGE sign_jup sign_jup VARCHAR(5) DEFAULT NULL, CHANGE sign_sat sign_sat VARCHAR(5) DEFAULT NULL, CHANGE sign_ura sign_ura VARCHAR(5) DEFAULT NULL, CHANGE sign_nep sign_nep VARCHAR(5) DEFAULT NULL, CHANGE sign_plu sign_plu VARCHAR(5) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chains CHANGE date date VARCHAR(12) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE orb_lun orb_lun INT NOT NULL, CHANGE orb_mer orb_mer INT NOT NULL, CHANGE orb_wen orb_wen INT NOT NULL, CHANGE orb_mar orb_mar INT NOT NULL, CHANGE orb_jup orb_jup INT NOT NULL, CHANGE orb_sat orb_sat INT NOT NULL, CHANGE orb_ura orb_ura INT NOT NULL, CHANGE orb_nep orb_nep INT NOT NULL, CHANGE orb_plu orb_plu INT NOT NULL, CHANGE sign_sol sign_sol VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_lun sign_lun VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_mer sign_mer VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_wen sign_wen VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_mar sign_mar VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_jup sign_jup VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_sat sign_sat VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_ura sign_ura VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_nep sign_nep VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE sign_plu sign_plu VARCHAR(5) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
