<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200420142654 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE aspect_indexes (id INT AUTO_INCREMENT NOT NULL, planet_id_1 INT DEFAULT NULL, planet_id_2 INT DEFAULT NULL, day_id INT DEFAULT NULL, aspect_id INT DEFAULT NULL, INDEX IDX_1EF947B04674CED4 (planet_id_1), INDEX IDX_1EF947B0DF7D9F6E (planet_id_2), INDEX IDX_1EF947B09C24126 (day_id), INDEX IDX_1EF947B098507F8C (aspect_id), UNIQUE INDEX IDX_ASPECT_PLANET_DAYS (planet_id_1, planet_id_2, day_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE aspect_indexes ADD CONSTRAINT FK_1EF947B04674CED4 FOREIGN KEY (planet_id_1) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aspect_indexes ADD CONSTRAINT FK_1EF947B0DF7D9F6E FOREIGN KEY (planet_id_2) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aspect_indexes ADD CONSTRAINT FK_1EF947B09C24126 FOREIGN KEY (day_id) REFERENCES days (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aspect_indexes ADD CONSTRAINT FK_1EF947B098507F8C FOREIGN KEY (aspect_id) REFERENCES aspects (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE aspect_indexes');
    }
}
