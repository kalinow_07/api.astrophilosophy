<?php

namespace App\Model\Chain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanetRepository")
 * @ORM\Table(name="planets")
 */
class Planet
{

    public function __construct()
    {
        $this->signs = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $marker;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="planet")
     */
    private $signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="wla_planet")
     */
    private $wla_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="ekz_planet")
     */
    private $ekz_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="pad_planet")
     */
    private $pad_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="izg_planet")
     */
    private $izg_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="net_planet")
     */
    private $net_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="wra_1_planet")
     */
    private $wra_1_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="wra_2_planet")
     */
    private $wra_2_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="wra_3_planet")
     */
    private $wra_3_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="rod_1_planet")
     */
    private $rod_1_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="rod_2_planet")
     */
    private $rod_2_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="rod_3_planet")
     */
    private $rod_3_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="wla_2_planet")
     */
    private $wla_2_signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Chain\Entity\Sign", mappedBy="izg_2_planet")
     */
    private $izg_2_signs;






    /**
     * @ORM\Column(type="integer", length=1, options={"default" : 0})
     */
    public $private;


    /**
     * @ORM\Column(type="integer", length=1, options={"default" : 0})
     */
    public $social;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5, options={"default" : 0})
     */
    public $avg_speed;

    /**
     * @return mixed
     */
    public function getSigns(): Collection
    {
        return $this->signs;
    }

    /**
     * @param mixed $planets
     */
    public function setSigns($signs): void
    {
        $this->signs = $signs;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMarker(): ?string
    {
        return $this->marker;
    }

    public function setMarker(string $marker): self
    {
        $this->marker = $marker;

        return $this;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getWlaSigns()
    {
        return $this->wla_signs;
    }

    /**
     * @param mixed $wla_signs
     */
    public function setWlaSigns($wla_signs): void
    {
        $this->wla_signs = $wla_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getEkzSigns()
    {
        return $this->ekz_signs;
    }

    /**
     * @param mixed $ekz_signs
     */
    public function setEkzSigns($ekz_signs): void
    {
        $this->ekz_signs = $ekz_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getPadSigns()
    {
        return $this->pad_signs;
    }

    /**
     * @param mixed $pad_signs
     */
    public function setPadSigns($pad_signs): void
    {
        $this->pad_signs = $pad_signs;
    }
    /**
     * @return Collection|Sign[]
     */
    public function getIzgSigns()
    {
        return $this->izg_signs;
    }

    /**
     * @param mixed $izg_signs
     */
    public function setIzgSigns($izg_signs): void
    {
        $this->izg_signs = $izg_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getNetSigns()
    {
        return $this->net_signs;
    }

    /**
     * @param mixed $net_signs
     */
    public function setNetSigns($net_signs): void
    {
        $this->net_signs = $net_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getWra1Signs()
    {
        return $this->wra_1_signs;
    }

    /**
     * @param mixed $wra_1_signs
     */
    public function setWra1Signs($wra_1_signs): void
    {
        $this->wra_1_signs = $wra_1_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getWra2Signs()
    {
        return $this->wra_2_signs;
    }

    /**
     * @param mixed $wra_2_signs
     */
    public function setWra2Signs($wra_2_signs): void
    {
        $this->wra_2_signs = $wra_2_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getWra3Signs()
    {
        return $this->wra_3_signs;
    }

    /**
     * @param mixed $wra_3_signs
     */
    public function setWra3Signs($wra_3_signs): void
    {
        $this->wra_3_signs = $wra_3_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getRod1Signs()
    {
        return $this->rod_1_signs;
    }

    /**
     * @param mixed $rod_1_signs
     */
    public function setRod1Signs($rod_1_signs): void
    {
        $this->rod_1_signs = $rod_1_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getRod2Signs()
    {
        return $this->rod_2_signs;
    }

    /**
     * @param mixed $rod_2_signs
     */
    public function setRod2Signs($rod_2_signs): void
    {
        $this->rod_2_signs = $rod_2_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getRod3Signs()
    {
        return $this->rod_3_signs;
    }

    /**
     * @param mixed $rod_3_signs
     */
    public function setRod3Signs($rod_3_signs): void
    {
        $this->rod_3_signs = $rod_3_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getWla2Signs()
    {
        return $this->wla_2_signs;
    }

    /**
     * @param mixed $wla_2_signs
     */
    public function setWla2Signs($wla_2_signs): void
    {
        $this->wla_2_signs = $wla_2_signs;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getIzg2Signs()
    {
        return $this->izg_2_signs;
    }

    /**
     * @param mixed $izg_2_signs
     */
    public function setIzg2Signs($izg_2_signs): void
    {
        $this->izg_2_signs = $izg_2_signs;
    }




}
