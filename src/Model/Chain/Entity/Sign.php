<?php

namespace App\Model\Chain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SignRepository")
 * @ORM\Table(name="signs")
 */
class Sign
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $marker;

    /**
     * @ORM\Column(type="integer")
     */
    private $degree_from;

    /**
     * @ORM\Column(type="integer")
     */
    private $degree_to;

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="signs")
     * @ORM\JoinColumn(name="planet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="wla_signs")
     * @ORM\JoinColumn(name="wla_planet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $wla_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="ekz_signs")
     * @ORM\JoinColumn(name="ekz_planet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $ekz_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="pad_signs")
     * @ORM\JoinColumn(name="pad_planet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $pad_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="izg_signs")
     * @ORM\JoinColumn(name="izg_planet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $izg_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="net_signs")
     * @ORM\JoinColumn(name="net_planet_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $net_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="wra_1_signs")
     * @ORM\JoinColumn(name="wra_planet_1_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $wra_1_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="wra_2_signs")
     * @ORM\JoinColumn(name="wra_planet_2_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $wra_2_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="wra_3_signs")
     * @ORM\JoinColumn(name="wra_planet_3_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $wra_3_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="rod_1_signs")
     * @ORM\JoinColumn(name="rod_planet_1_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $rod_1_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="rod_2_signs")
     * @ORM\JoinColumn(name="rod_planet_2_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $rod_2_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="rod_3_signs")
     * @ORM\JoinColumn(name="rod_planet_3_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $rod_3_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="wla_2_signs")
     * @ORM\JoinColumn(name="wla_planet_2_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $wla_2_planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="izg_2_signs")
     * @ORM\JoinColumn(name="izg_planet_2_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $izg_2_planet;


    /**
     * @return mixed
     */
    public function getPlanet()
    {
        return $this->planet;
    }

    /**
     * @param mixed $sign_id
     */
    public function setPlanet($planet): void
    {
        $this->planet = $planet;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMarker(): ?string
    {
        return $this->marker;
    }

    public function setMarker(string $marker): self
    {
        $this->marker = $marker;

        return $this;
    }

    public function getDegreeFrom(): ?int
    {
        return $this->degree_from;
    }

    public function setDegreeFrom(int $degree_from): self
    {
        $this->degree_from = $degree_from;

        return $this;
    }

    public function getDegreeTo(): ?int
    {
        return $this->degree_to;
    }

    public function setDegreeTo(int $degree_to): self
    {
        $this->degree_to = $degree_to;

        return $this;
    }


    public function getPlanetByChainType($type) : Planet
    {

        switch ($type) {
            case "wla":
                return $this->wla_planet;
                break;
            case "ekz":
                return $this->ekz_planet;
                break;
            case "pad":
                return $this->pad_planet;
                break;
            case "izg":
                return $this->izg_planet;
                break;
        }

        throw new Exception("Не определен тип цепочки");

    }

    public function gett(){
        return 123;
    }
}
