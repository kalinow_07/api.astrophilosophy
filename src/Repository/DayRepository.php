<?php

namespace App\Repository;

use App\Model\Chain\Entity\Day;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Day|null find($id, $lockMode = null, $lockVersion = null)
 * @method Day|null findOneBy(array $criteria, array $orderBy = null)
 * @method Day[]    findAll()
 * @method Day[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DayRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Day::class);
    }

    // /**
    //  * @return Day[] Returns an array of Day objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Day
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function getEm()
    {
        return $this->_em;
    }

    public function getDayByDate(Carbon $carbon){

        return $this->findOneBy([
            "date" => $carbon->format("d.m.Y")
        ]);
    }
    public function getAllDates(){

        return $this->createQueryBuilder('d')
            ->select("d.date")
            ->andWhere('d.id > :val')
            ->setParameter('val', 3)
            ->orderBy('d.id', 'ASC')
            //->setMaxResults(1000)
            ->getQuery()
            ->getResult();
    }
    public function getCountByRange($date_from, $date_to){

        $qb = $this->_em->createQueryBuilder();

        $result = $this->createQueryBuilder('d')
            ->select("d.date")
            ->andWhere('d.id > :val')
            ->andWhere($qb->expr()->gte('d.date_format', ':date_from'))
            ->andWhere($qb->expr()->lte('d.date_format', ':date_to'))
            ->setParameters([
                'val'=> 3,
                'date_from'=> $date_from,
                'date_to'=> $date_to,
            ])
            ->orderBy('d.id', 'ASC')
            //->setMaxResults(1000)
            ->getQuery()
            ->getResult();
        return count($result);
    }
}
