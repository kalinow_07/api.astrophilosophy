<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200418140708 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE planet_indexes (id INT AUTO_INCREMENT NOT NULL, planet_id INT DEFAULT NULL, sign_id INT DEFAULT NULL, day_id INT DEFAULT NULL, speed NUMERIC(10, 5) NOT NULL, INDEX IDX_CFB1BFD5A25E9820 (planet_id), INDEX IDX_CFB1BFD56FC7C15 (sign_id), INDEX IDX_CFB1BFD59C24126 (day_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE planet_indexes ADD CONSTRAINT FK_CFB1BFD5A25E9820 FOREIGN KEY (planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE planet_indexes ADD CONSTRAINT FK_CFB1BFD56FC7C15 FOREIGN KEY (sign_id) REFERENCES signs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE planet_indexes ADD CONSTRAINT FK_CFB1BFD59C24126 FOREIGN KEY (day_id) REFERENCES days (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE planet_indexes');
    }
}
