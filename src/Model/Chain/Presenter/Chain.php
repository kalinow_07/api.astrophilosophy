<?php

namespace App\Model\Chain\Presenter;

class Chain
{

    private $chain;
    private $nodes;
    private $links;
    private $aspects;



    public function get($chain){

        $this->chain = $chain;

        $this->getNodes();
        $this->getLinks();
        $this->getAspects();

        return [
            "nodes" => $this->nodes,
            "links" => $this->links,
            "aspects" => $this->aspects,
            "chain_count" =>  1,
        ];

    }

    private function getNodes(){
        foreach ($this->chain["planets"] as $item){
            $this->nodes[] = [
                "id" => $item->marker . "1",
                "marker" => $item->marker,
                "sign" => $item->sign . "1",
                "sign_img" => $item->sign . ".png",
                "sign_name" => $item->sign_name,
                "name" => $item->name,
                "chain" => 1,
                "img" => $item->marker . ".png",
                "bal" => $item->bal,
                "speed" => round($item->speed, 0),
                "center" => $item->center,
                "degree" => round($item->degree, 2),
                "retro" => $item->retrograde,
                "orbit" => $item->orbit,
            ];
        }
    }


    private function getLinks(){

        foreach ($this->chain["planets"] as $item){
            $this->links[] = [
                "source" => $item->marker . "1",
                "target" => $item->parent . "1",
                "distance" => 80,
                "chain" => true,
                "relation" => false,
            ];
        }

    }


    private function getAspects(){

        foreach ($this->chain["aspects"] as $item){

            $this->aspects[] = [
                "source" => $item["planet1"] . "1",
                "target" => $item["planet2"] . "1",
                "type" => $item["id"],
                "name" => $item["name"],
                "img" => "aspect_" . $item["id"] . ".png",
            ];
        }
    }




}
