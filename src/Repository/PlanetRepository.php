<?php

namespace App\Repository;

use App\Model\Chain\Entity\Planet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Planet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planet[]    findAll()
 * @method Planet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Planet::class);
    }

    // /**
    //  * @return Planet[] Returns an array of Planet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Planet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getPublicList()
    {

        $planet_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        return $this->createQueryBuilder('p')
            ->where("p.id IN(:planet_ids)")
            ->setParameter('planet_ids', $planet_ids)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(15)
            ->getQuery()
            ->getResult();
    }

    public function getPrivatePlanets()
    {

        return $this->createQueryBuilder('p')
            ->where("p.private = 1")
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function getSocialPlanets()
    {

        return $this->createQueryBuilder('p')
            ->where("p.social = 1")
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function getByMarker($marker){
        return $this->createQueryBuilder('s')
            ->andWhere('s.marker = :marker')
            ->setParameter('marker', $marker)
            ->getQuery()
            ->getOneOrNullResult();
    }


}
