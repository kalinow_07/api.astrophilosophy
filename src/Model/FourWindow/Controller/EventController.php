<?php

namespace App\Model\FourWindow\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\FourWindow\UseCase\EventWindow\EventWindow;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForOrbit;
use App\Repository\PlanetRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends AbstractController
{
    /**
     * @Route("/four_window/event/get", name="four_window_event_get")
     */
    public function index(Request $request, EventWindow $eventWindow)
    {
        try{

            $param = $request->query->all();

            $result = $eventWindow->get($param);

            $response = new JsonResponse($result);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }
}
