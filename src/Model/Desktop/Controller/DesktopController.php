<?php

namespace App\Model\Desktop\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\Service\Orbit;
use App\Model\Chain\UseCase\Chain;
use App\Model\Desktop\UseCase\SearchDateByCondition;
use App\Repository\DayRepository;
use App\Repository\PlanetRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Chain\Presenter\Chain as ChainPresenter;

class DesktopController extends AbstractController
{

    private $presenter;

    public function __construct(ChainPresenter $presenter)
    {
        $this->presenter = $presenter;

    }

    /**
     * @Route("/desktop/search_date_by_condition", name="search_date_by_condition")
     */
    public function searchDateByCondition(Request $request,
                                          SearchDateByCondition $searchDateByCondition,
                                          DayRepository $dayRepository,
                                          PlanetRepository $planetRepository,
                                          SignRepository $signRepository, Orbit $orbit)
    {

        $param = $request->query->all();

        $result = $searchDateByCondition->searchDateByCondition(json_decode($param["conditions"]), $param);
        if(count($result) > 0){
            $day = $dayRepository->find($result[0]["day_id"]);
            $date = Carbon::parse($day->getDate());
            $result = [
                "year" => (int)$date->format("Y"),
                "month" => (int)$date->format("m"),
                "day" => (int)$date->format("d"),
                "hour" => (int)12,
                "minute" => (int)0,
            ];
        }else{
            $result = false;
        }

        return $this->json([
            "date" => $result,
            "planets" => $planetRepository->findAll(),
            "signs" => $signRepository->findAll(),
            "orbits" => $orbit->all(),
        ]);

    }
}
