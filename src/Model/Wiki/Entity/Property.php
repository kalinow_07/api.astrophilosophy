<?php

namespace App\Model\Wiki\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Model\Wiki\Repository\PropertyRepository")
 * @ORM\Table(name="properties",indexes={@Index(name="property_idx", columns={"property"})})
 */

class Property
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */

    public $property;
    /**
     * @ORM\Column(type="string", length=255)
     */
    public $name;




}