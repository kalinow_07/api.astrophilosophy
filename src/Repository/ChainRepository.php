<?php

namespace App\Repository;

use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Model\Chain\Entity\Chain;

/**
 * @method Chain|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chain|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chain[]    findAll()
 * @method Chain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChainRepository extends ServiceEntityRepository
{


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Chain::class);
    }

    // /**
    //  * @return Chain[] Returns an array of Chain objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chain
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getChainByDate(Carbon $carbon){

        return $this->findOneBy([
            "date" => $carbon->format("d.m.Y")
        ]);
    }

    public function findOneOrCreate($date)
    {
        $date = Carbon::parse($date);

        $entity = $this->findOneBy(
            ['date' => $date,]
        );

        if (null === $entity)
        {
            $entity = new Chain();
            $entity->setDate($date);
            $this->_em->persist($entity);
            $this->_em->flush();
        }

        return $entity;
    }

    public function getEm(){
        return $this->_em;
    }
}
