<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191117082927 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signs ADD net_planet_id INT DEFAULT NULL, ADD wra_planet_1_id INT DEFAULT NULL, ADD wra_planet_2_id INT DEFAULT NULL, ADD wra_planet_3_id INT DEFAULT NULL, ADD rod_planet_1_id INT DEFAULT NULL, ADD rod_planet_2_id INT DEFAULT NULL, ADD rod_planet_3_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81923FB4E FOREIGN KEY (net_planet_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81456C0115 FOREIGN KEY (wra_planet_1_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E8157D9AEFB FOREIGN KEY (wra_planet_2_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81EF65C99E FOREIGN KEY (wra_planet_3_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81BA8C40BA FOREIGN KEY (rod_planet_1_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E81A839EF54 FOREIGN KEY (rod_planet_2_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signs ADD CONSTRAINT FK_41946E8110858831 FOREIGN KEY (rod_planet_3_id) REFERENCES planets (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_41946E81923FB4E ON signs (net_planet_id)');
        $this->addSql('CREATE INDEX IDX_41946E81456C0115 ON signs (wra_planet_1_id)');
        $this->addSql('CREATE INDEX IDX_41946E8157D9AEFB ON signs (wra_planet_2_id)');
        $this->addSql('CREATE INDEX IDX_41946E81EF65C99E ON signs (wra_planet_3_id)');
        $this->addSql('CREATE INDEX IDX_41946E81BA8C40BA ON signs (rod_planet_1_id)');
        $this->addSql('CREATE INDEX IDX_41946E81A839EF54 ON signs (rod_planet_2_id)');
        $this->addSql('CREATE INDEX IDX_41946E8110858831 ON signs (rod_planet_3_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81923FB4E');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81456C0115');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E8157D9AEFB');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81EF65C99E');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81BA8C40BA');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E81A839EF54');
        $this->addSql('ALTER TABLE signs DROP FOREIGN KEY FK_41946E8110858831');
        $this->addSql('DROP INDEX IDX_41946E81923FB4E ON signs');
        $this->addSql('DROP INDEX IDX_41946E81456C0115 ON signs');
        $this->addSql('DROP INDEX IDX_41946E8157D9AEFB ON signs');
        $this->addSql('DROP INDEX IDX_41946E81EF65C99E ON signs');
        $this->addSql('DROP INDEX IDX_41946E81BA8C40BA ON signs');
        $this->addSql('DROP INDEX IDX_41946E81A839EF54 ON signs');
        $this->addSql('DROP INDEX IDX_41946E8110858831 ON signs');
        $this->addSql('ALTER TABLE signs DROP net_planet_id, DROP wra_planet_1_id, DROP wra_planet_2_id, DROP wra_planet_3_id, DROP rod_planet_1_id, DROP rod_planet_2_id, DROP rod_planet_3_id');
    }
}
