<?php

namespace App\Model\Chain\UseCase;


use App\Repository\DayRepository;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;

class Day
{

    private $date;
    private $sub_date;
    private $dayRepository;
    private $day;
    private $sub_day;
    private $degree;
    private $sub_degree;
    private $logger;

    public function __construct(DayRepository $dayRepository, LoggerInterface $logger)
    {
        $this->dayRepository = $dayRepository;
        $this->logger = $logger;
    }

    public function get(Carbon $date, Carbon $sub_date){
        $this->date = $date;
        $this->sub_date = $sub_date;
        $this->day = $this->dayRepository->getDayByDate($date);
        $this->sub_day = $this->dayRepository->getDayByDate($this->sub_date);

        return $this;
    }

    public function getDegree(\App\Model\Chain\Entity\Planet $planet){

        $this->degree = $this->day->getDegree($planet);
        $this->sub_degree = $this->sub_day->getDegree($planet);


        // Корректировка градусов в зависимости от секунд
        $this->correctionDegree();

        return $this->degree;
    }

    private function correctionDegree(){

        // Корректировка по минутам
        $correction_seconds = -3.6*60*60;

        // Разница
        $different = $this->sub_degree - $this->degree;

        // Ретро
        $retro = false;
        if($different > 0) $retro = true;

        // Если на стыке градусов, то реверс
        if(abs($different) > 200){
            $different = ($different - 360) * -1;
            $retro = !$retro;
        }

        // Скорость, градусов в секунду
        $speed = abs((float)$different / (24 * 60 * 60));

        // Сколько прошло минут с начала дня
        $seconds_of_day = ($this->date->timestamp - $this->date->copy()->startOfday()->timestamp);


        if($retro){
            $this->degree = $this->degree - (($correction_seconds + $seconds_of_day) * $speed);
        }else{
            $this->degree = $this->degree + (($correction_seconds + $seconds_of_day) * $speed);
        }

        if($this->degree > 360){
            $this->degree = 359.999999;
        }
        if($this->degree < 0){
            $this->degree = 0.000001;
        }

    }

}
