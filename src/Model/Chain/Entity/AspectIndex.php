<?php

namespace App\Model\Chain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AspectIndexRepository")
 * @ORM\Table(name="aspect_indexes", uniqueConstraints={@ORM\UniqueConstraint(name="IDX_ASPECT_PLANET_DAYS", columns={"planet_id_1", "planet_id_2", "day_id", "aspect_id"})})
 * @UniqueEntity(
 *     fields={"planet_id_1", "planet_id_2", "day_id", "aspect_id"},
 *     errorPath="planet_day_chain_type_uniq",
 *     message="Error uniq planet and day and, chain_type."
 * )
 */
class AspectIndex
{


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="aspect_indexes")
     * @ORM\JoinColumn(name="planet_id_1", referencedColumnName="id", onDelete="CASCADE")
     */
    private $planet_1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="aspect_indexes")
     * @ORM\JoinColumn(name="planet_id_2", referencedColumnName="id", onDelete="CASCADE")
     */
    private $planet_2;


    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Day", inversedBy="orbit_indexes")
     * @ORM\JoinColumn(name="day_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $day;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Aspect", inversedBy="orbit_indexes")
     * @ORM\JoinColumn(name="aspect_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $aspect;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPlanet1()
    {
        return $this->planet_1;
    }

    /**
     * @param mixed $planet_1
     */
    public function setPlanet1($planet_1): void
    {
        $this->planet_1 = $planet_1;
    }

    /**
     * @return mixed
     */
    public function getPlanet2()
    {
        return $this->planet_2;
    }

    /**
     * @param mixed $planet_2
     */
    public function setPlanet2($planet_2): void
    {
        $this->planet_2 = $planet_2;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day): void
    {
        $this->day = $day;
    }

    /**
     * @return mixed
     */
    public function getAspect()
    {
        return $this->aspect;
    }

    /**
     * @param mixed $aspect
     */
    public function setAspect($aspect): void
    {
        $this->aspect = $aspect;
    }




}
