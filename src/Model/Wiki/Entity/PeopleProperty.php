<?php

namespace App\Model\Wiki\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Model\Wiki\Repository")
 * @ORM\Table(name="people_property",indexes={@Index(name="people_idx", columns={"people_id"}),@Index(name="value_idx", columns={"value_id"}),@Index(name="property_idx", columns={"property"})})
 */

class PeopleProperty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Wiki\Entity\People", inversedBy="people_property")
     * @ORM\JoinColumn(name="people_id", referencedColumnName="id", onDelete="RESTRICT")
     */
    private $people_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Wiki\Entity\Value", inversedBy="values")
     * @ORM\JoinColumn(name="value_id", referencedColumnName="id", onDelete="RESTRICT")
     */
    private $value_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $property;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $column1;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $column2;

}