<?php

namespace App\Command;

use App\Model\Chain\UseCase\Chain;
use App\Model\Chain\Entity\Chain as ChainModel;
use App\Repository\ChainRepository;
use App\Repository\DayRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FillChainCommand extends Command
{

    private $chainRepository;
    private $chain;
    private $dayRepository;

    public function __construct(ChainRepository $chainRepository,
                                Chain $chain,
                                DayRepository $dayRepository)
    {
        $this->chain = $chain;
        $this->chainRepository = $chainRepository;
        $this->dayRepository = $dayRepository;
        parent::__construct();
    }


    protected static $defaultName = 'fill_chain';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
//        $arg1 = $input->getArgument('arg1');
//
//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }
//
//        if ($input->getOption('option1')) {
//            // ...
//        }
//
//        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        $console_start = $output->section();
        $console_progress = $output->section();
        $console_finish = $output->section();


        $console_start->writeln('Начало заполнения таблицы');

        $days = $this->dayRepository->getAllDates();
        $count = count($days);

        foreach ($days as $key => $day){
            $console_progress->overwrite(round($key * 100 / $count, 2));
//            $console_progress->writeln($day);
            $chainModel = $this->chainRepository->findOneOrCreate($day["date"]);

            $chain = $this->chain->build(Carbon::parse($day["date"]));

            foreach ($chain as $planet){
//                $console_progress->writeln($planet->marker);
                $method_orb = "setOrb" . ucfirst($planet->marker);
                $chainModel->$method_orb($planet->orbit);
                $method_sign = "setSign" . ucfirst($planet->marker);
                $chainModel->$method_sign($planet->sign);
                $this->chainRepository->getEm()->persist($chainModel);
            }


            $this->chainRepository->getEm()->flush();
            $this->chainRepository->getEm()->clear(); // Detaches all objects from Doctrine!



        }

        $console_finish->writeln('Готово!');

    }
}
