<?php

namespace App\Model\FourWindow\UseCase\DoublePlanetWindow;

use App\Model\Chain\Entity\Planet;
use App\Model\FourWindow\Controller\PlanetController;
use App\Model\FourWindow\Service\ConvertPlanetName;
use App\Repository\PlanetRepository;
use App\Repository\SignRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DoublePlanetWindowForSign
{
    /**
     * @Assert\NotBlank
     */
    public $first_planet_id;
    /**
     * @Assert\NotBlank
     */
    public $second_planet_id;
    /**
     * @Assert\NotBlank
     */
    public $sign_id;

    public $entityManager;
    public $validator;
    private $connection;
    private $planetRepository;
    private $signRepository;
    private $convertPlanetName;
    private $first_column;
    private $second_column;
    private $sign_marker;

    public function __construct(EntityManagerInterface $entityManager,
                                ValidatorInterface $validator,
                                PlanetRepository $planetRepository,
                                SignRepository $signRepository,
                                ConvertPlanetName $convertPlanetName)
    {

        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->connection = $this->entityManager->getConnection();
        $this->planetRepository = $planetRepository;
        $this->signRepository = $signRepository;
        $this->convertPlanetName = $convertPlanetName;

    }

    public function get(array $param){

        if(isset($param["first_planet_id"])){
            $this->first_planet_id = $param["first_planet_id"];
        }
        if(isset($param["second_planet_id"])){
            $this->second_planet_id = $param["second_planet_id"];
        }
        if(isset($param["sign_id"])){
            $this->sign_id = $param["sign_id"];
        }

        $this->validation();

        $first_planet = $this->planetRepository->find($this->first_planet_id);
        if(!(bool)$first_planet) throw new Exception("First planet not find");

        $second_planet = $this->planetRepository->find($this->second_planet_id);
        if(!(bool)$second_planet) throw new Exception("Second planet not find");

        $sign = $this->signRepository->find($this->sign_id);
        if(!(bool)$sign) throw new Exception("Sign not find");


        $first_column = "sign_" . $first_planet->getMarker();
        $second_column = "sign_" . $second_planet->getMarker();
        $sign_marker = $sign->getMarker();
        $this->first_column = $first_column;
        $this->second_column = $second_column;
        $this->sign_marker = $sign_marker;

        $sql = "
        SELECT $first_column, $second_column, COUNT(*) count FROM chains
        WHERE $first_column = '$sign_marker'
        GROUP BY $second_column
        LIMIT 20
        ";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();

        return [
            "items" => $this->presenter($stmt->fetchAll())
        ];

    }

    private function validation(){
        $errors = $this->validator->validate($this);
        if(count($errors) > 0){
            throw new Exception($errors[0]->getMessage());
        }
    }

    private function presenter($items){

        $first_column = $this->first_column;
        $second_column = $this->second_column;
        $sign_marker = $this->sign_marker;
        foreach ($items as $key => $item){
            $items[$key]["name"] = $this->convertPlanetName->getPlanetName($first_column)
                . " в знаке " . $this->convertPlanetName->getSignName($item[$first_column])
                . ", а " . $this->convertPlanetName->getPlanetName($second_column)
                . " в знаке " .  $this->convertPlanetName->getSignName($item[$second_column]);
            $sign = $this->signRepository->getByMarker($item[$second_column]);
            $items[$key]["sign_id"] = $sign->getId();
        }
        return $items;
    }

}