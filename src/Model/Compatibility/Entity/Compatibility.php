<?php

namespace App\Model\Compatibility\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompatibilityRepository")
 * @ORM\Table(name="compatibilities")
 */
class Compatibility
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $wikibase_item1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $birthday_date1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $wikibase_item2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $birthday_date2;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getWikibaseItem1()
    {
        return $this->wikibase_item1;
    }

    /**
     * @param mixed $wikibase_item1
     */
    public function setWikibaseItem1($wikibase_item1): void
    {
        $this->wikibase_item1 = $wikibase_item1;
    }

    /**
     * @return mixed
     */
    public function getBirthdayDate1()
    {
        return $this->birthday_date1;
    }

    /**
     * @param mixed $birthday_date1
     */
    public function setBirthdayDate1($birthday_date1): void
    {
        $this->birthday_date1 = $birthday_date1;
    }

    /**
     * @return mixed
     */
    public function getWikibaseItem2()
    {
        return $this->wikibase_item2;
    }

    /**
     * @param mixed $wikibase_item2
     */
    public function setWikibaseItem2($wikibase_item2): void
    {
        $this->wikibase_item2 = $wikibase_item2;
    }

    /**
     * @return mixed
     */
    public function getBirthdayDate2()
    {
        return $this->birthday_date2;
    }

    /**
     * @param mixed $birthday_date2
     */
    public function setBirthdayDate2($birthday_date2): void
    {
        $this->birthday_date2 = $birthday_date2;
    }


}
