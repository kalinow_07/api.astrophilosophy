<?php

namespace App\Model\Statistic\PlanetStatistic\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Model\FourWindow\UseCase\EventWindow\EventWindow;
use App\Model\FourWindow\UseCase\PlanetWindow\PlanetWindowForOrbit;
use App\Model\Statistic\PlanetStatistic\UseCase\GetPlanetStatistic;
use App\Model\Statistic\PlanetStatistic\UseCase\GetPlanetStatistic\Request\Command;
use App\Model\Statistic\Service\ChainCondition\ChainCondition;
use App\Model\Wiki\Entity\CategoryProperty;
use App\Model\Wiki\Entity\Value;
use App\Repository\PlanetRepository;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlanetStatisticController extends AbstractController
{

    /**
     * @Route("/planet_statistic/get", name="planet_statistic_get", methods={"GET"})
     */
    public function getStatistic(Request $request, GetPlanetStatistic $getPlanetStatistic)
    {
        try{

            $chain_condition_input = $request->query->get("chain_condition");
            $chain_condition = new ChainCondition($chain_condition_input);
            $result = $getPlanetStatistic->get($chain_condition);

            $response = new JsonResponse($result);
            //$response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;

        }catch (Exception $exception){
            return $this->json(["error" => $exception->getMessage()]);
        }
    }

}
