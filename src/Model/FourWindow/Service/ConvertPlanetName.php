<?php
namespace App\Model\FourWindow\Service;


use App\Repository\PlanetRepository;
use App\Repository\SignRepository;

class ConvertPlanetName
{

    private $planetRepository;
    private $signRepository;

    private $planets;
    private $signs;

    public function __construct(PlanetRepository $planetRepository, SignRepository $signRepository)
    {
        $this->planetRepository = $planetRepository;
        $this->signRepository = $signRepository;
        $this->planets = $this->planetRepository->findAll();
        $this->signs = $this->signRepository->findAll();
    }

    public function getPlanetName($string){

        $result = $string;

        foreach ($this->planets as $planet){
            $marker = $planet->getMarker();
            if(strripos($string, $marker) !== false){
                $result = $planet->getName();
            }
        }

        return $result;
    }

    public function getSignName($string){

        $result = $string;

        foreach ($this->signs as $sign){
            $marker = $sign->getMarker();
            if(strripos($string, $marker) !== false){
                $result = $sign->getName();
            }
        }

        return $result;
    }
    public function getOrbitName($string){

        $result = "";

        switch ($string){
            case 0: $result = " в центре"; break;
            case 1: $result = " на первой орбите"; break;
            case 2: $result = " на второй орбите"; break;
            case 3: $result = " на третьей орбите"; break;
            case 4: $result = " на четвертой орбите"; break;
            case 5: $result = " на пятой орбите"; break;
            case 6: $result = " на шестой орбите"; break;
            case 7: $result = " на седьмой орбите"; break;
            case 8: $result = " на восьмой орбите"; break;
            case 9: $result = " на девятой орбите"; break;
        }


        return $result;
    }


}