<?php

namespace App\Model\Chain\UseCase;

use App\Repository\SignRepository;
use App\Model\Chain\Entity\Planet as PlanetEntity;

class Planet
{

    private $signRepository;

    public function __construct(SignRepository $signRepository)
    {
        $this->signRepository = $signRepository;

    }

    public function getParent($degree, $chain_type){
        return $this->signRepository->findOneByDegree($degree)->getPlanetByChainType($chain_type)->getMarker();
    }
    public function getSign($degree){
        return $this->signRepository->findOneByDegree($degree);
    }
}