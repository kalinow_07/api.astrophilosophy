<?php


namespace App\Model\Statistic\Service\ChainCondition;


use App\Model\Chain\UseCase\Chain;
use App\Repository\ChainTypeRepository;
use Carbon\Carbon;

class ChainConditionService implements ChainConditionInterface
{

    private $chain;
    private $chainUseCase;
    private $conditions = [];
    private $chain_condition;
    private $chainTypeRepository;


    public function __construct(Chain $chain_use_case, ChainTypeRepository $chainTypeRepository)
    {
        $this->chainUseCase = $chain_use_case;
        $this->chainTypeRepository = $chainTypeRepository;
    }

    public function getConditionByDate(ChainCondition $chainCondition)
    {

        $this->chain_condition = $chainCondition;

        $this->conditions["by_sign"] = $chainCondition->getBySign();
        $this->conditions["by_orbit"] = $chainCondition->getByOrbit();
        $this->conditions["by_aspect"] = $chainCondition->getByAspect();
        $this->conditions["date_from"] = $chainCondition->getDateFrom();
        $this->conditions["date_to"] = $chainCondition->getDateTo();

        $this->buildChain();
        $this->conditions["chain"] = $this->chain;
        $this->retrieveChainType();
        $this->retrievePlanets();
        $this->generateMessage();

        return $this->conditions;
//        $planets = [
//            "sol" => [
//                "id" => 1,
//                "orbit" => 3,
//                "sign_id" => 3,
//                "aspect" => [
//                    "planet_id_1" => 1,
//                    "planet_id_2" => 2,
//                    "aspect_id" => 2,
//                ],
//            ],
//        ];

    }


    private function buildChain(){
        $this->chain = $this->chainUseCase->build(Carbon::parse($this->chain_condition->getDate()));
    }

    private function retrieveChainType(){
        $chainType = $this->chainTypeRepository->getByName($this->chain_condition->getChainType());
        $this->conditions["chain_type_id"] = $chainType->getId();
    }


    private function retrievePlanets(){
        $this->conditions["planets"] = [];
        $planets = $this->chain_condition->getPlanets();

        foreach ($this->chain["planets"] as $planet){
            if(!in_array($planet->marker, $planets)) continue;

            // Определение аспектов
            $aspects = [];
            foreach ($this->chain["aspects"] as $aspect){
                if($planet->marker == $aspect["planet1"] || $planet->marker == $aspect["planet2"] ){
                    $aspects[] = $aspect;
                }
            }

            $condition_planet = [
                "planet_name" => $planet->name,
                "sign_name" => $planet->sign_name,
                "marker" => $planet->marker,
                "id" => $planet->id,
                "orbit" => $planet->orbit,
                "sign_id" => $planet->sign_id,
                "aspects" => $aspects,
            ];

            $this->conditions["planets"][] = $condition_planet;

        }
    }


    private function generateMessage(){
        $message = "";
        foreach ($this->conditions["planets"] as $key => $planet){
            $planet_name = $planet["planet_name"];
            $orbit = $planet["orbit"];
            if($planet["orbit"] == 0){
                $orbit_name = "в центре";
            }else{
                $orbit_name = "на $orbit орбите";
            }

            $sign_name = "";
            if($this->conditions["by_sign"] == true){
                $sign = $planet["sign_name"];
                $sign_name = "и в знаке $sign";
            }

            $plus = "+";
            if($key == 0) $plus = "";
            $message .= " $plus Планета $planet_name $orbit_name $sign_name";
        }
        $this->conditions["message"] = $message;
    }











}
