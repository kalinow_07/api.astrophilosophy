<?php

namespace App\Model\Compatibility\UseCase;

use App\Model\Chain\UseCase\Chain;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;

class Compatibility
{


    private $date_first;
    private $date_second;
    private $compatibility = [];

    private $chain;
    private $entityManagerInterface;

    public function __construct(Chain $chain, EntityManagerInterface $entityManager)
    {

        $this->chain = $chain;
        $this->entityManagerInterface = $entityManager;

    }

    public function build(Carbon $date_first, Carbon $date_second){

        $this->date_first = $date_first;
        $this->date_second = $date_second;
        $this->compatibility["date_first"] = $this->date_first;
        $this->compatibility["date_second"] = $this->date_second;

        $this->getGive();
        $this->getPrivateInner();
        $this->getSocialInner();
        $this->getPrivateOuter();
        $this->getSocialOuter();
        $this->rating();

        return $this->compatibility;

    }

    private function getPrivateInner(){

        // Центральные личные планеты первой даты.
        $first_private_central_planets = [];
        foreach ($this->chain->build($this->date_first) as $first_planet){
            if($first_planet->orbit == 0 && $first_planet->private == 1){
                $first_private_central_planets[] = $first_planet->marker;
            }
        }

        $result = [];
        foreach ($this->chain->build($this->date_second) as $second_planet){
            if($second_planet->orbit == 0 && $second_planet->private == 1 && in_array($second_planet->marker, $first_private_central_planets)){
                $result[] = $second_planet;
            }
        }

        return $this->compatibility["private_inner_planets"] = $first_private_central_planets;


    }


    private function getSocialInner(){
        // Центральные личные планеты первой даты.
        $first_private_central_planets = [];
        foreach ($this->chain->build($this->date_first) as $first_planet){
            if($first_planet->orbit == 0 && $first_planet->social == 1){
                $first_private_central_planets[] = $first_planet->marker;
            }
        }

        $result = [];
        foreach ($this->chain->build($this->date_second) as $second_planet){
            if($second_planet->orbit == 0 && $second_planet->social == 1 && in_array($second_planet->marker, $first_private_central_planets)){
                $result[] = $second_planet;
            }
        }

        return $this->compatibility["social_inner_planets"] = $first_private_central_planets;
    }


    private function getPrivateOuter(){
        // Центральные личные планеты первой даты.
        $first_private_central_planets = [];
        foreach ($this->chain->build($this->date_first) as $first_planet){
            if($first_planet->orbit != 0 && $first_planet->private == 1){
                $first_private_central_planets[] = $first_planet->marker;
            }
        }

        $result = [];
        foreach ($this->chain->build($this->date_second) as $second_planet){
            if($second_planet->orbit != 0 && $second_planet->private == 1 && in_array($second_planet->marker, $first_private_central_planets)){
                $result[] = $second_planet;
            }
        }

        return $this->compatibility["private_outer_planets"] = $first_private_central_planets;
    }

    private function getSocialOuter(){
        // Центральные личные планеты первой даты.
        $first_private_central_planets = [];
        foreach ($this->chain->build($this->date_first) as $first_planet){
            if($first_planet->orbit != 0 && $first_planet->social == 1){
                $first_private_central_planets[] = $first_planet->marker;
            }
        }

        $result = [];
        foreach ($this->chain->build($this->date_second) as $second_planet){
            if($second_planet->orbit != 0 && $second_planet->social == 1 && in_array($second_planet->marker, $first_private_central_planets)){
                $result[] = $second_planet;
            }
        }

        return $this->compatibility["social_outer_planets"] = $first_private_central_planets;
    }


    private function getGive(){

        $first_give_planets = $this->getGivePlanets($this->date_first, $this->date_second);
        $second_give_planets = $this->getGivePlanets($this->date_second, $this->date_first);

        $this->compatibility["give"] = [];
        $this->compatibility["give"]["first"] = $first_give_planets;
        $this->compatibility["give"]["second"] = $second_give_planets;

    }

    private function getGivePlanets($date_first, $date_second){

        // Центральные планеты от первого участника
        $first_central_planets = [];
        foreach ($this->chain->build($date_first) as $first_planet){
            if($first_planet->orbit == 0){
                $first_central_planets[] = $first_planet->marker;
            }
        }

        $result = [];
        foreach ($this->chain->build($date_second) as $second_planet){
            if($second_planet->orbit == 0) continue;
            if(in_array($second_planet->marker, $first_central_planets)){
                $result[] = $second_planet;
            }
        }
        return $this->compatibility["give_planets"] = $result;
    }

    private function rating(){
        $give_rating = (count($this->compatibility["give"]["first"]) * 3) + (count($this->compatibility["give"]["second"]) * 3);
        $private_inner_rating = (count($this->compatibility["private_inner_planets"]) * 3) * -1;
        $social_inner_rating = (count($this->compatibility["social_inner_planets"]) * 2);
        $private_outer_rating = (count($this->compatibility["private_outer_planets"]) * 2) * -1;
        $social_outer_rating = count($this->compatibility["social_outer_planets"]) * -1;

        $this->compatibility["rating"] = [];
        $this->compatibility["rating"]["give_rating"] = $give_rating;
        $this->compatibility["rating"]["private_inner_rating"] = $private_inner_rating;
        $this->compatibility["rating"]["social_inner_rating"] = $social_inner_rating;
        $this->compatibility["rating"]["private_outer_rating"] = $private_outer_rating;
        $this->compatibility["rating"]["social_outer_rating"] = $social_outer_rating;
        $this->compatibility["rating"]["sum"] = $give_rating + $private_inner_rating + $social_inner_rating + $private_outer_rating + $social_outer_rating;
        $this->compatibility["count_planets"] = [];
        $this->compatibility["count_planets"]["give"] = count($this->compatibility["give"]["first"]) + count($this->compatibility["give"]["second"]);
        $this->compatibility["count_planets"]["private_inner"] = count($this->compatibility["private_inner_planets"]);
        $this->compatibility["count_planets"]["social_inner"] = count($this->compatibility["social_inner_planets"]);
        $this->compatibility["count_planets"]["private_outer"] = count($this->compatibility["private_outer_planets"]);
        $this->compatibility["count_planets"]["social_outer"] = count($this->compatibility["social_outer_planets"]);

    }
}