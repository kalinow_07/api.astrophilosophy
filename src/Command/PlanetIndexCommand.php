<?php

namespace App\Command;

use App\Model\Chain\Entity\PlanetIndex;
use App\Model\Chain\UseCase\Chain;
use App\Model\Chain\Entity\Chain as ChainModel;
use App\Repository\ChainRepository;
use App\Repository\DayRepository;
use App\Repository\PlanetIndexRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PlanetIndexCommand extends Command
{

    private $chainRepository;
    private $chain;
    private $dayRepository;
    private $planetIndexRepository;
    private $signRepository;

    public function __construct(ChainRepository $chainRepository,
                                Chain $chain,
                                DayRepository $dayRepository,
                                PlanetIndexRepository $planetIndexRepository,
                                SignRepository $signRepository)
    {
        $this->chain = $chain;
        $this->chainRepository = $chainRepository;
        $this->dayRepository = $dayRepository;
        $this->planetIndexRepository = $planetIndexRepository;
        $this->signRepository = $signRepository;
        parent::__construct();
    }


    protected static $defaultName = 'planet_index';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('date', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $console_start = $output->section();
        $console_progress = $output->section();
        $console_finish = $output->section();


        $console_start->writeln('Начало заполнения таблицы planet_indexes');

        $days = $this->dayRepository->getAllDates();
        $count = count($days);

        foreach ($days as $key => $date){

            if($key < 200) continue;

            $input_date = Carbon::parse($input->getArgument('date'))->timestamp;
            if(Carbon::parse($date["date"])->timestamp < $input_date) continue;

            $console_progress->overwrite(round($key * 100 / $count, 2) . "/" . $date["date"]);

            $day = $this->dayRepository->getDayByDate(Carbon::parse($date["date"]));



            $chain = $this->chain->build(Carbon::parse($day->getDate()));

            foreach ($chain["planets"] as $planet){

                //if($this->planetIndexRepository->hasByPlanetAndDay($planet, $day)) continue;

                $planet_index = new PlanetIndex();
                $planet_index->setPlanet($planet);
                $planet_index->setDay($day);
                $planet_index->setSpeed($planet->avg_speed);
                $planet_index->setSign($this->signRepository->getByMarker($planet->sign));
                $this->planetIndexRepository->getEm()->persist($planet_index);
            }


            if (($key % 365) == 0){
                $this->planetIndexRepository->getEm()->flush();
            }

        }

        $this->planetIndexRepository->getEm()->flush();

        $console_finish->writeln('Готово!');

    }
}
