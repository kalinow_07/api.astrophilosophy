<?php

namespace App\Command;

use App\Model\Chain\Entity\AspectIndex;
use App\Model\Chain\Entity\OrbitIndex;
use App\Model\Chain\Entity\PlanetIndex;
use App\Model\Chain\UseCase\Chain;
use App\Model\Chain\Entity\Chain as ChainModel;
use App\Repository\AspectIndexRepository;
use App\Repository\AspectRepository;
use App\Repository\ChainRepository;
use App\Repository\ChainTypeRepository;
use App\Repository\DayRepository;
use App\Repository\OrbitIndexRepository;
use App\Repository\PlanetIndexRepository;
use App\Repository\PlanetRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AspectIndexCommand extends Command
{

    private $chainRepository;
    private $chain;
    private $dayRepository;
    private $planetIndexRepository;
    private $orbitIndexRepository;
    private $signRepository;
    private $chainTypeRepository;
    private $aspectIndexRepository;
    private $planetRepository;
    private $aspectRepository;

    public function __construct(ChainRepository $chainRepository,
                                Chain $chain,
                                DayRepository $dayRepository,
                                PlanetIndexRepository $planetIndexRepository,
                                SignRepository $signRepository,
                                ChainTypeRepository $chainTypeRepository,
                                OrbitIndexRepository $orbitIndexRepository,
                                AspectIndexRepository $aspectIndexRepository,
                                PlanetRepository $planetRepository,
                                AspectRepository $aspectRepository)
    {
        $this->chain = $chain;
        $this->chainRepository = $chainRepository;
        $this->dayRepository = $dayRepository;
        $this->planetIndexRepository = $planetIndexRepository;
        $this->signRepository = $signRepository;
        $this->chainTypeRepository = $chainTypeRepository;
        $this->orbitIndexRepository = $orbitIndexRepository;
        $this->aspectIndexRepository = $aspectIndexRepository;
        $this->planetRepository = $planetRepository;
        $this->aspectRepository = $aspectRepository;
        parent::__construct();
    }


    protected static $defaultName = 'aspect_index';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('date', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $console_start = $output->section();
        $console_progress = $output->section();
        $console_finish = $output->section();

        $this->orbitIndexRepository->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->dayRepository->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->chainTypeRepository->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->aspectIndexRepository->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);


        $console_start->writeln('Начало заполнения таблицы orbit_indexes');

        $days = $this->dayRepository->getAllDates();

        $count = count($days);

        foreach ($days as $key => $date){

            $input_date = Carbon::parse($input->getArgument('date'))->timestamp;

            if(Carbon::parse($date["date"])->timestamp < $input_date) continue;

            $console_progress->overwrite(round($key * 100 / $count, 2) . "/" . $date["date"]);

            $day = $this->dayRepository->getDayByDate(Carbon::parse($date["date"]));


            $chain = $this->chain->build(Carbon::parse($day->getDate()));

//            $console_start->writeln(json_encode($chain));

            // Некоторые аспекты дублируются
            $double_aspects = [];

            foreach ($chain["aspects"] as $aspect){

                $planet_1 = $this->planetRepository->getByMarker($aspect["planet1"]);
                $planet_2 = $this->planetRepository->getByMarker($aspect["planet2"]);
                $aspect = $this->aspectRepository->getByMarker($aspect["aspect"]);

//                // Пропуск повторяющихся аспектов
//                if(in_array($planet_1->getMarker() . "_" . $planet_2->getMarker(), $double_aspects)){
//                    continue;
//                }
//                $double_aspects[] = $planet_1->getMarker() . "_" . $planet_2->getMarker();

                if($this->aspectIndexRepository->hasByPlanetAndDay($planet_1, $planet_2, $day, $aspect)) continue;

                $aspect_index = new AspectIndex();
                $aspect_index->setPlanet1($planet_1);
                $aspect_index->setPlanet2($planet_2);
                $aspect_index->setDay($day);
                $aspect_index->setAspect($aspect);
                $this->aspectIndexRepository->getEm()->persist($aspect_index);
            }


            if (($key % 50) == 0){
                $this->aspectIndexRepository->getEm()->flush();
                $this->aspectIndexRepository->getEm()->clear();
            }

        }

        $this->aspectIndexRepository->getEm()->flush();

        $console_finish->writeln('Готово!');

    }
}
