<?php

namespace App\Model\FourWindow\UseCase\PlanetWindow;

use App\Model\Chain\Entity\Planet;
use App\Model\FourWindow\Controller\PlanetController;
use App\Model\FourWindow\Service\ConvertPlanetName;
use App\Repository\PlanetRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PlanetWindowForOrbit
{

    /**
     * @Assert\NotBlank
     */
    public $planet_id;
    /**
     * @Assert\NotBlank
     */
    public $date_from;
    /**
     * @Assert\NotBlank
     */
    public $date_to;

    public $entityManager;
    public $validator;
    private $connection;
    private $planetRepository;
    private $convertPlanetName;
    private $column;

    public function __construct(EntityManagerInterface $entityManager,
                                ValidatorInterface $validator,
                                PlanetRepository $planetRepository,
                                ConvertPlanetName $convertPlanetName)
    {

        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->connection = $this->entityManager->getConnection();
        $this->planetRepository = $planetRepository;
        $this->convertPlanetName = $convertPlanetName;

    }

    public function get(array $param){

        if(isset($param["planet_id"])){
            $this->planet_id = $param["planet_id"];
        }
        if(isset($param["date_from"])){
            $this->date_from = $param["date_from"];
        }
        if(isset($param["date_to"])){
            $this->date_to = $param["date_to"];
        }

        $this->validation();

        $planet = $this->planetRepository->find($this->planet_id);

        if(!(bool)$planet) throw new Exception("Planet not find");

        $column = "orb_" . $planet->getMarker();
        $this->column = $column;

        $sql = "
        SELECT $column, COUNT(*) count FROM chains
        WHERE date >= :date_from AND date <= :date_to
        GROUP BY $column
        LIMIT 20
        ";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            "date_from" => $this->date_from,
            "date_to" => $this->date_to,
        ]);

        return [
            "items" => $this->presenter($stmt->fetchAll())
        ];

    }

    private function validation(){
        $errors = $this->validator->validate($this);
        if(count($errors) > 0){
            throw new Exception($errors[0]->getMessage());
        }
    }

    private function presenter($items){

        foreach ($items as $key => $item){
            $column = $this->column;
            $items[$key]["name"] = $this->convertPlanetName->getPlanetName($column) . $this->convertPlanetName->getOrbitName($item[$column]);
        }
        return $items;
    }

}