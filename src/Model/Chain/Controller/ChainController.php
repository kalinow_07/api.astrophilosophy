<?php

namespace App\Model\Chain\Controller;

use App\Model\Chain\Entity\Day;
use App\Model\Chain\UseCase\Chain;
use App\Repository\PlanetRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Chain\Presenter\Chain as ChainPresenter;

class ChainController extends AbstractController
{

    private $presenter;

    public function __construct(ChainPresenter $presenter)
    {
        $this->presenter = $presenter;

    }

    /**
     * @Route("/chain/get", name="chain_get")
     */
    public function index(Request $request, Chain $chain)
    {

        $param = $request->query->all();

        if(!isset($param["date"])){
            $datetime = Carbon::now();
        }else{
            $datetime = urldecode($param["date"]);
            $datetime = Carbon::parse($datetime);
        }

        $type = "wla";
        if(isset($param["type"])){
            $type = $param["type"];
        }

        $result = $chain->build($datetime, $type);


        return $this->json([
            'result' => $this->presenter->get($result),
            "param" => $param
        ]);

    }
}
