<?php

namespace App\Model\Chain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AspectRepository")
 * @ORM\Table(name="aspects")
 */
class Aspect
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $marker;

    /**
     * @ORM\Column(type="integer")
     */
    private $degree;

    /**
     * @ORM\Column(type="integer")
     */
    private $inaccuracy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * @param mixed $marker
     */
    public function setMarker($marker): void
    {
        $this->marker = $marker;
    }

    /**
     * @return mixed
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * @param mixed $degree
     */
    public function setDegree($degree): void
    {
        $this->degree = $degree;
    }

    /**
     * @return mixed
     */
    public function getInaccuracy()
    {
        return $this->inaccuracy;
    }

    /**
     * @param mixed $inaccuracy
     */
    public function setInaccuracy($inaccuracy): void
    {
        $this->inaccuracy = $inaccuracy;
    }


}
