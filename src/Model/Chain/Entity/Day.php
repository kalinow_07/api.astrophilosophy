<?php

namespace App\Model\Chain\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DayRepository")
 * @ORM\Table(name="days",indexes={@Index(name="date_idx", columns={"date"})})
 */
class Day
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $sol;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $lun;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $mer;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $wen;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $mar;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $jup;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $sat;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $ura;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $nep;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $plu;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $use1;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $use2;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $date;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_format;

    public function get(){
        return [
            "id" => $this->id,
            "sol" => $this->sol,
            "lun" => $this->lun,
            "mer" => $this->mer,
            "wen" => $this->wen,
            "mar" => $this->mar,
            "jup" => $this->jup,
            "sat" => $this->sat,
            "ura" => $this->ura,
            "nep" => $this->nep,
            "plu" => $this->plu,
            "use1" => $this->use1,
            "use2" => $this->use2,
            "date" => $this->date,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSol()
    {
        return $this->sol;
    }

    public function setSol($sol): self
    {
        $this->sol = $sol;

        return $this;
    }

    public function getLun()
    {
        return $this->lun;
    }

    public function setLun($lun): self
    {
        $this->lun = $lun;

        return $this;
    }

    public function getMer()
    {
        return $this->mer;
    }

    public function setMer($mer): self
    {
        $this->mer = $mer;

        return $this;
    }

    public function getWen()
    {
        return $this->wen;
    }

    public function setWen($wen): self
    {
        $this->wen = $wen;

        return $this;
    }

    public function getMar()
    {
        return $this->mar;
    }

    public function setMar($mar): self
    {
        $this->mar = $mar;

        return $this;
    }

    public function getJup()
    {
        return $this->jup;
    }

    public function setJup($jup): self
    {
        $this->jup = $jup;

        return $this;
    }

    public function getSat()
    {
        return $this->sat;
    }

    public function setSat($sat): self
    {
        $this->sat = $sat;

        return $this;
    }

    public function getUra()
    {
        return $this->ura;
    }

    public function setUra($ura): self
    {
        $this->ura = $ura;

        return $this;
    }

    public function getNep()
    {
        return $this->nep;
    }

    public function setNep($nep): self
    {
        $this->nep = $nep;

        return $this;
    }

    public function getPlu()
    {
        return $this->plu;
    }

    public function setPlu($plu): self
    {
        $this->plu = $plu;

        return $this;
    }

    public function getUse1()
    {
        return $this->use1;
    }

    public function setUse1($use1): self
    {
        $this->use1 = $use1;

        return $this;
    }

    public function getUse2()
    {
        return $this->use2;
    }

    public function setUse2($use2): self
    {
        $this->use2 = $use2;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDegree(Planet $planet){
        $marker = $planet->getMarker();
        return $this->$marker;
    }

}
