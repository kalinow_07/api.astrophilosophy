<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 23.06.19
 * Time: 19:50
 */

namespace App\Model\Chain\UseCase;

use App\Model\Chain\Entity\Planet;
use Symfony\Component\Config\Definition\Exception\Exception;

class Orbit
{

    private $result_array = []; // Результирующий массив

    private $times = 20; // Сколько раз запустить функцию определения орбиты.
    private $time_iterations = 1;

    private $planets = [];

    public function __construct(array $planets)
    {
        foreach ($planets as $planet) {
            $this->planets[$planet->marker] = $planet->parent;
        }
    }

    public function get(Planet $planet)
    {

        $this->time_iterations = 1;
        $this->result_array = [];
        $this->iteration($planet->marker);

        return $this->getOrbitNumber();

    }

    private function iteration($planet_marker)
    {
        $this->time_iterations++;
        if ($this->time_iterations > $this->times) return true;
        $this->addToResultArray($planet_marker);
        $this->iteration($this->planets[$planet_marker]);

    }

    private function addToResultArray($planet_marker)
    {

        // Если в нет в массиве, то добавляем новый элемент, иначе прибавляем единицу
        if (!isset($this->result_array[$planet_marker])) {
            $this->result_array[$planet_marker] = 1;
        } else {
            $this->result_array[$planet_marker]++;
        }
    }

    private function getOrbitNumber()
    {

        $first_key = array_key_first($this->result_array);

        if (!isset($this->result_array[$first_key])) return null;

        // Если первый элемент != 1, значит планет в центре
        if ($this->result_array[$first_key] != 1) return 0;

        // Количество элементров со значением 1
        $counts = array_count_values($this->result_array);
        return $counts[1];
    }


}