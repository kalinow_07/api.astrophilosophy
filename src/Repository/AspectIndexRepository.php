<?php

namespace App\Repository;

use App\Model\Chain\Entity\Aspect;
use App\Model\Chain\Entity\AspectIndex;
use App\Model\Chain\Entity\ChainType;
use App\Model\Chain\Entity\Day;
use App\Model\Chain\Entity\OrbitIndex;
use App\Model\Chain\Entity\Planet;
use App\Model\Chain\Entity\PlanetIndex;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AspectIndex|null find($id, $lockMode = null, $lockVersion = null)
 * @method AspectIndex|null findOneBy(array $criteria, array $orderBy = null)
 * @method AspectIndex[]    findAll()
 * @method AspectIndex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AspectIndexRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AspectIndex::class);
    }

    // /**
    //  * @return Planet[] Returns an array of Planet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Planet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function getEm()
    {
        return $this->_em;
    }

    public function hasByPlanetAndDay(Planet $planet1, Planet $planet2, Day $day, Aspect $aspect){

            return (bool) $this->findOneBy(
                ['planet_1' => $planet1, 'planet_2' => $planet2, 'day' => $day, 'aspect' => $aspect]
            );

    }
}
