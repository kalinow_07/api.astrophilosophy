<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190625183342 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE properties (id INT AUTO_INCREMENT NOT NULL, property VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, INDEX property_idx (property), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE people_property (id INT AUTO_INCREMENT NOT NULL, people_id INT NOT NULL, value_id INT NOT NULL, property VARCHAR(255) NOT NULL, column1 VARCHAR(255) NOT NULL, column2 VARCHAR(255) NOT NULL, INDEX people_idx (people_id), INDEX value_idx (value_id), INDEX property_idx (property), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE peoples (id INT AUTO_INCREMENT NOT NULL, pageid INT NOT NULL, title VARCHAR(255) NOT NULL, wikibase_item VARCHAR(255) NOT NULL, column1 VARCHAR(255) NOT NULL, column2 VARCHAR(255) NOT NULL, column3 VARCHAR(255) NOT NULL, birthday VARCHAR(255) NOT NULL, date_of_death VARCHAR(255) NOT NULL, height VARCHAR(255) NOT NULL, birthday_date DATETIME NOT NULL, INDEX date_idx (birthday_date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE value (id INT AUTO_INCREMENT NOT NULL, wikibase_item VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, column1 VARCHAR(255) NOT NULL, column2 VARCHAR(255) NOT NULL, name_rus VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE properties');
        $this->addSql('DROP TABLE people_property');
        $this->addSql('DROP TABLE peoples');
        $this->addSql('DROP TABLE value');
    }
}
