<?php

namespace App\Model\Chain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanetIndexRepository")
 * @ORM\Table(name="planet_indexes", uniqueConstraints={@ORM\UniqueConstraint(name="IDX_PLANET_DAY", columns={"planet_id", "day_id"})})
 * @UniqueEntity(
 *     fields={"planet_id", "day_id"},
 *     errorPath="planet_day_uniq",
 *     message="Error uniq planet and day."
 * )
 */
class PlanetIndex
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    public $speed;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Planet", inversedBy="planet_indexes")
     * @ORM\JoinColumn(name="planet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $planet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Sign", inversedBy="planet_indexes")
     * @ORM\JoinColumn(name="sign_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $sign;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param mixed $speed
     */
    public function setSpeed($speed): void
    {
        $this->speed = $speed;
    }

    /**
     * @return mixed
     */
    public function getPlanet()
    {
        return $this->planet;
    }

    /**
     * @param mixed $planet
     */
    public function setPlanet($planet): void
    {
        $this->planet = $planet;
    }

    /**
     * @return mixed
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * @param mixed $sign
     */
    public function setSign($sign): void
    {
        $this->sign = $sign;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day): void
    {
        $this->day = $day;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Chain\Entity\Day", inversedBy="planet_indexes")
     * @ORM\JoinColumn(name="day_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $day;

}
