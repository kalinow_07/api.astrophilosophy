<?php

namespace App\Command;

use App\Model\Chain\Entity\PlanetIndex;
use App\Model\Chain\UseCase\Chain;
use App\Model\Chain\Entity\Chain as ChainModel;
use App\Model\Wiki\Repository\PeopleRepository;
use App\Repository\ChainRepository;
use App\Repository\DayRepository;
use App\Repository\PlanetIndexRepository;
use App\Repository\SignRepository;
use Carbon\Carbon;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateImagesCommand extends Command
{

    private $peopleRepository;

    public function __construct(PeopleRepository $peopleRepository)
    {
        $this->peopleRepository = $peopleRepository;
        parent::__construct();
    }


    protected static $defaultName = 'update_images';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $console_start = $output->section();
        $console_progress = $output->section();
        $console_progress2 = $output->section();
        $console_finish = $output->section();

        $console_start->writeln('Старт!');

        $people = $this->peopleRepository->getAll();
        $count = count($people);


        foreach ($people as $key => $item){

            if((int)$item["id"] < 275700) continue;

            $person = $this->peopleRepository->find($item["id"]);
            $console_progress->overwrite(round($key * 100 / $count, 2) . "/" . $person->getId());

            $title = urlencode($person->getTitle());
            $url = "https://ru.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles=$title";

            $content = file_get_contents($url);
            $content_array = json_decode($content, true);

            if(isset($content_array["query"]["pages"])){

                $key_page = 0;
                foreach ($content_array["query"]["pages"] as $page){

                    if($key_page > 0) continue;

                    if(isset($page["original"])){
                        if(isset($page["original"]["source"])){
                            $person->setImage($page["original"]["source"]);
                        }

                    }

                    $key_page++;

                    $this->peopleRepository->getEm()->persist($person);

                }
            }

            if (($key % 100) == 0){
                $this->peopleRepository->getEm()->flush();
                $this->peopleRepository->getEm()->clear();
                unset($person);
            }

        }


        $console_finish->writeln('Готово!');

    }
}
